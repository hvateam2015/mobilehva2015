package nl.mirabeau.miralunch.models;

import java.util.ArrayList;
import java.util.List;

/**
 * @author David Kooijman
 *	Counter object - used for the headcount output
 */
public class Counter {
	private List<Integer> counter = new ArrayList<Integer>();
	
	public Counter(){
	}
	
	/**
	 * Add a certain amount to the counter object.
	 * @param amount - Integer with the amount to add
	 */
	public void addToCounter(int amount){
		counter.add(amount);
	}
	
	/**
	 * The total amount gathered from the list.
	 * @return result - Integer with the total count.
	 */
	public int getCount(){
		int result = 0 ;
		for(int count : counter){
			result += count;
		};
		return result;
	}
}
