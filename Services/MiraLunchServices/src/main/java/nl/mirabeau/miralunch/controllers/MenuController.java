package nl.mirabeau.miralunch.controllers;

import java.sql.Date;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import nl.mirabeau.miralunch.models.Day;
import nl.mirabeau.miralunch.models.Week;
import nl.mirabeau.miralunch.constants.URIConstants;
import nl.mirabeau.miralunch.daos.MenuDAO;

/**
 * Handles requests For the Employee service.
 */
@RestController
@RequestMapping("/menu")
@CrossOrigin
public class MenuController {
	
	MenuDAO menuDAO = new MenuDAO();
	
	@CrossOrigin
	@RequestMapping(value = URIConstants.GET_WEEK_YEAR, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Week> getWeekYear(HttpServletRequest request, HttpServletResponse response, @PathVariable("location") String location, @PathVariable("week") int weekNr, @PathVariable("year") int year) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Access-Control-Allow-Origin", "*");
		
		//Database get week based on weekNr, location and year.
		Week week = menuDAO.getMenu(location, weekNr, year);
		
		Calendar cal = Calendar.getInstance();
		int checkWeek = cal.get(cal.WEEK_OF_YEAR);
		int checkYear = cal.get(cal.YEAR);
		
		//if week doesn't exist, create new!
		if(week.getDays() == null){
			
			// Huidig jaar is kleiner of gelijk aan gevraagd jaar (gevraagd jaar is groter dus mag door)
			if(checkYear <= year){
				// Huidige week is groter dan Week (ouder dus return null)
				if(checkYear == year && checkWeek > weekNr){
					return null;
				}

				week = menuDAO.createMenu(location, weekNr, year);
			} else {
				// Huidig jaar is groter (dus gevraagd is oud, maar geen nieuwe)
				return null;
			}
		}
		
		return new ResponseEntity<Week>(week, HttpStatus.ACCEPTED);
	}
	
	@CrossOrigin
	@RequestMapping(value = URIConstants.POST_WEEK_YEAR, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> postWeekYear(HttpServletRequest request, HttpServletResponse response, @PathVariable("location") String location, @PathVariable("week") int weekNr, @PathVariable("year") int year) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Access-Control-Allow-Origin", "*");
		
		// get week from database;
		
		String monDish = request.getParameter(Day.DAY_PARAMETER_MON);
		String tueDish = request.getParameter(Day.DAY_PARAMETER_TUE);
		String wedDish = request.getParameter(Day.DAY_PARAMETER_WED);
		String thuDish = request.getParameter(Day.DAY_PARAMETER_THU);
		String friDish = request.getParameter(Day.DAY_PARAMETER_FRI);
		Week week = menuDAO.getMenu(location, weekNr, year);
		
		Day[] listOfDays = week.getDays();
		listOfDays[0].setDish(monDish);
		listOfDays[1].setDish(tueDish);
		listOfDays[2].setDish(wedDish);
		listOfDays[3].setDish(thuDish);
		listOfDays[4].setDish(friDish);
		week.setDays(listOfDays);
		
		menuDAO.updateWeek(week);
		
		return new ResponseEntity<String>("Saved week" + week, HttpStatus.ACCEPTED);
		
	}
}