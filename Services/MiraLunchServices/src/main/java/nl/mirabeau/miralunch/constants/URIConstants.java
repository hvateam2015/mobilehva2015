package nl.mirabeau.miralunch.constants;

 
/**
 * @author David
 * Standardizing for link structure. All the calls will be in here so a change in link will affect all
 * url structures. 
 */
public class URIConstants {
   //Global GET Constants
    public static final String GET_OBJECT = "/{id}";
    public static final String GET_ALL_ITEMS = "/all";
    public static final String GET_ITEM_TODAY = "/today";
    
    //Global POST Constants
    public static final String ADD_OBJECT = "/add";
    public static final String EDIT_OBJECT = "/edit";
    
    //Global CREATE Constants
   public static final String CREATE_DUMMY = "/create/dummy";
   public static final String CREATE_OBJECT = "/create";
   public static final String CREATE_WEEK = "/create/week/";
   
   //Global DELETE Constants
   public static final String DELETE_OBJECT = "/delete/{id}";
   
   //Menu Constants
   public static final String GET_WEEK_YEAR = "get/{location}/{week}/{year}";
   public static final String POST_WEEK_YEAR = "post/{location}/{week}/{year}";
   
   //User Constants
   public static final String CHECK_USER_AUTH = "/{userName}/{passWord}";
}