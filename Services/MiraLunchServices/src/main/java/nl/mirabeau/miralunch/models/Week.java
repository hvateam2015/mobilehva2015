package nl.mirabeau.miralunch.models;

import java.io.Serializable;
import java.util.Arrays;
 
public class Week implements Serializable{
 
    private static final long serialVersionUID = -7788619177798333712L;
    
    public static final String WEEK_COLLECTION = "week";
    public static final String WEEK_NR = "weekNr";
    public static final String WEEK_YEAR = "year";
    public static final String WEEK_LOCATION = "location";
    public static final String WEEK_DAYS = "days";
    
    private int weekNr;
    private int year;
    private String location;
    private Day[] days;
    
    public Week(){}
     
    public Week(int weekNr, int year, Day[] days) {
		this.weekNr = weekNr;
		this.year = year;
		this.days = days;
	}

    public int getWeekNr() {
        return weekNr;
    }
    public void setWeekNr(int weekNr) {
        this.weekNr = weekNr;
    }

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public Day[] getDays() {
		return days;
	}

	public void setDays(Day[] meals) {
		this.days = meals;
	}
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Menu [weekNr=" + weekNr + ", year=" + year + ", location=" + location + ", days=" + Arrays.toString(days) + "]";
	}
    
}