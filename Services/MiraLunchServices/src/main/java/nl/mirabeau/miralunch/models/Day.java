package nl.mirabeau.miralunch.models;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class Day implements Serializable {

	private static final long serialVersionUID = -7788619177798333712L;
	
    public static final String DAY_DATE = "date";
    public static final String DAY_DISH = "dish";

    public static final String DAY_PARAMETER_MON = "ma";
    public static final String DAY_PARAMETER_TUE = "di";
    public static final String DAY_PARAMETER_WED = "wo";
    public static final String DAY_PARAMETER_THU = "do";
    public static final String DAY_PARAMETER_FRI = "vr";

	private Date date;
	private String dish;

	// Constructors
	public Day() {
	}

	public Day(long date, String dish) {
		super();
		this.dish = dish;
		this.date = new Date(date);
	}

	public Day(Timestamp timestamp, String dish) {
		super();
		this.dish = dish;
		this.date = timestamp;
	}

	public String getDish() {
		return dish;
	}

	public void setDish(String dish) {
		this.dish = dish;
	}

	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date){
		this.date = date;
	}

	@Override
	public String toString() {
		return "date=" + date + ", meal [dish=" + dish + "]";
	}
}