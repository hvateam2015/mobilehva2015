package nl.mirabeau.miralunch.controllers;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;

import nl.mirabeau.miralunch.dbconnection.DbConfiguration;
import nl.mirabeau.miralunch.dbconnection.DbConnection;

@RestController
@RequestMapping("/mongo")
public class MongoTestController {
	private static final Logger logger = LoggerFactory.getLogger(HeadCountController.class);

	@RequestMapping(value = "test-connection", method = RequestMethod.GET)
	public @ResponseBody String testConnection() {
		logger.info("Start MongoDB connection test");

		String output = "";

		output += "Database name: " + DbConfiguration.DB_NAME + "<br />";
		output += "Connection url: " + DbConfiguration.DB_URL + "<br />" + "<br />";

		output += "Settings configured right" + "<br />" + "<br />";

		DbConnection con = new DbConnection();

		MongoDatabase db = con.getConnection();

		output += "Connected to the database<br />";

		FindIterable<Document> iterable = db.getCollection("headcount")
				.find(new Document("date", 1448323200).append("location", con.capitalizeWord("Hoorn")));
		output += "Iterable found<br />";

		// logs debug message
		if (logger.isDebugEnabled()) {
			logger.debug("LOGGER DOET HET!");
		}
		logger.error("This is Error message", new Exception("Testing"));
		logger.info("testtesttest");

		Integer total = 0;
		
		iterable.forEach(new Block<Document>() {
			@Override
			public void apply(final Document document) {
//				addGuests(document.get("count"));
				logger.debug("BAM: " + document);
			}
		});

		return output;
	}
}
