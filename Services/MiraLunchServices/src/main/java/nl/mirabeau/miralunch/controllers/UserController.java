package nl.mirabeau.miralunch.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import nl.mirabeau.miralunch.models.User;
import nl.mirabeau.miralunch.constants.URIConstants;

/**
 * Handles requests for the Employee service.
 */
@RestController
@RequestMapping("/user")
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	// Map to store Users, ideally we should use database
	Map<Integer, User> userData = new HashMap<Integer, User>();

	private UserController() {
		createTestData();
	}

	@RequestMapping(value = URIConstants.CHECK_USER_AUTH, method = RequestMethod.GET)
	public @ResponseBody boolean isLoginConfirmed(@PathVariable("userName") String userName,
			@PathVariable("passWord") String passWord) {
		logger.info("Check AUTH for user=" + userName);

		Set<Integer> userIdKeys = userData.keySet();
		for (Integer i : userIdKeys) {
			System.out.println(userName);
			System.out.println(passWord);
			if (userData.get(i).getUserName().equals(userName) && userData.get(i).getPassWord().equals(passWord)) {
				return true;
			}
		}
		return false;
	}

	@RequestMapping(value = URIConstants.GET_OBJECT, method = RequestMethod.GET)
	public @ResponseBody User getUser(@PathVariable("id") int userId) {
		logger.info("Start getUser. ID=" + userId);

		return userData.get(userId);
	}

	@RequestMapping(value = URIConstants.GET_ALL_ITEMS, method = RequestMethod.GET)
	public @ResponseBody List<User> getAllUsers() {
		logger.info("Start getAllUsers.");
		List<User> users = new ArrayList<User>();
		Set<Integer> userIdKeys = userData.keySet();
		for (Integer i : userIdKeys) {
			users.add(userData.get(i));
		}
		return users;
	}

	@RequestMapping(value = URIConstants.CREATE_OBJECT, method = RequestMethod.POST)
	public @ResponseBody User createUser(@RequestBody User user) {
		logger.info("Start createUser.");
		user.setLocation("Hoorn");
		userData.put(user.getId(), user);
		return user;
	}

	@RequestMapping(value = URIConstants.DELETE_OBJECT, method = RequestMethod.PUT)
	public @ResponseBody User deleteUser(@PathVariable("id") int userId) {
		logger.info("Start deleteUser.");
		User user = userData.get(userId);
		userData.remove(userId);
		return user;
	}

	/**
	 * Create testobjects for the user, this will be deleted after the db
	 * connection
	 */
	private void createTestData() {
		int i = 1;
		User user = new User(i++, "lex", "lex321", "Hoorn");
		userData.put(user.getId(), user);
		user = new User(i++, "dees", "dees321", "Hoorn");
		userData.put(user.getId(), user);
		user = new User(i++, "david", "david321", "Hoorn");
		userData.put(user.getId(), user);
	}

}