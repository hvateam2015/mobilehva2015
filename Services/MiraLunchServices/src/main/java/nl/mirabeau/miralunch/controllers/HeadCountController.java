package nl.mirabeau.miralunch.controllers;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import nl.mirabeau.miralunch.models.HeadCount;
import nl.mirabeau.miralunch.constants.URIConstants;
import nl.mirabeau.miralunch.daos.HeadCountDAO;

/**
 * @author David
 *
 */
@RestController
@RequestMapping("/headcount")
public class HeadCountController {
	private static final Logger logger = LoggerFactory.getLogger(HeadCountController.class);
	private HeadCountDAO headCountDao = new HeadCountDAO();

	// Map to store employees, ideally we should use database
	Map<Integer, HeadCount> headCountData = new HashMap<Integer, HeadCount>();

	@RequestMapping(value = URIConstants.GET_ITEM_TODAY, method = RequestMethod.GET)
	public @ResponseBody int getDummyHeadCount() {
		logger.info("Start getDummyHeadCount");
		
		int totalCount = headCountDao.getHeadCount(1448323200, "hoorn");

		return totalCount;
	}

	@RequestMapping(value = URIConstants.GET_OBJECT, method = RequestMethod.GET)
	public @ResponseBody HeadCount getHeadCount(@PathVariable("id") int headCountId) {
		logger.info("Start getHeadCount. ID=" + headCountId);

		return headCountData.get(headCountId);
	}

	@RequestMapping(value = URIConstants.GET_ALL_ITEMS, method = RequestMethod.GET)
	public @ResponseBody List<HeadCount> getAllHeadCounts() {
		logger.info("Start getAllHeadCounts.");
		List<HeadCount> headCounts = new ArrayList<HeadCount>();
		Set<Integer> HeadCountIdKeys = headCountData.keySet();
		for (Integer i : HeadCountIdKeys) {
			headCounts.add(headCountData.get(i));
		}
		return headCounts;
	}

	@ResponseBody
	@RequestMapping(value = URIConstants.EDIT_OBJECT + "/update", method = RequestMethod.POST)
	public ResponseEntity<String> manipulateHeadcount(HttpServletRequest request, HttpServletResponse response) {

		HeadCount headcountObject = createHeadcountObject(request); 
		headCountDao.updateHeadcount(headcountObject); //puts a new document in the mongodb
				
		return new ResponseEntity<String>("Current headcount is modified.",
					HttpStatus.ACCEPTED);
		
	}
	
	@ResponseBody
	@RequestMapping(value = URIConstants.EDIT_OBJECT + "/delete", method = RequestMethod.POST)
	public ResponseEntity<String> deleteHeadcount(HttpServletRequest request, HttpServletResponse response) {

		headCountDao.deleteHeadcount(request.getParameter(HeadCount.key_deviceId)); 
		//deletes the document for a device-id
				
		return new ResponseEntity<String>("Your headcount is deleted.",
					HttpStatus.ACCEPTED);
		
	}
	
	
	   //returns a new HeadCount object. Which contains the values that needs to be stored in the MongoDB
	   private HeadCount createHeadcountObject(HttpServletRequest request){ 
		   
	    	//get headcount in integer
			String count = request.getParameter(HeadCount.key_count);
			int headcountUpdate = Integer.parseInt(count);		 //headcount
			
			//get deviceid in integer
			String deviceId = request.getParameter(HeadCount.key_deviceId); //device-id

			String location = request.getParameter(HeadCount.key_location);  //location
			long timestamp = getTimestamp();					//date
	
			HeadCount headcountObject = new HeadCount(deviceId, timestamp , location, headcountUpdate);

			return headcountObject;
	    }
	   
	   //returns the Timestamp in the right format.
	   public long getTimestamp(){
		    Calendar cal = Calendar.getInstance();
			
			cal.set(Calendar.MILLISECOND, 0);
			cal.set(Calendar.SECOND,  0);
			cal.set(Calendar.MINUTE,  0);
			cal.set(Calendar.HOUR, 0);
						
	        return cal.getTime().getTime();
	   }
	   
	}