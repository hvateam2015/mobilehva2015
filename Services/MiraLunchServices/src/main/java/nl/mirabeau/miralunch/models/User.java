package nl.mirabeau.miralunch.models;

import java.io.Serializable;
 
public class User implements Serializable{
 
    private static final long serialVersionUID = -7788619177798333712L;
     
    private int id;
    private String userName;
    private String passWord;
    private String location;
    
    public User(){}
    
    public User(int id, String userName, String passWord, String location){
    	this.id = id;
    	this.userName = userName;
    	this.passWord = passWord;
    	this.location = location;
    }
     
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getPassWord() {
        return passWord;
    }
    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", passWord=" + passWord + ", location=" + location + "]";
	}
     
    
}