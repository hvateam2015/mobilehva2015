package nl.mirabeau.miralunch.daos;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import nl.mirabeau.miralunch.dbconnection.DbConnection;
import nl.mirabeau.miralunch.models.HeadCount;
import nl.mirabeau.miralunch.models.Counter;

public class HeadCountDAO {
	private MongoDatabase db;
	private DbConnection con;
	private Counter counter;

	public HeadCountDAO() {
		con = new DbConnection();
		db = con.getConnection();
		counter = new Counter();
	}

	public int getHeadCount(int dayTimestamp, String location) {
		// totalCountForDay = 0;

		FindIterable<Document> iterable = db.getCollection("headcount")
				.find(new Document("date", dayTimestamp).append("location", con.capitalizeWord(location)));

		iterable.forEach(new Block<Document>() {
			@Override
			public void apply(final Document document) {
				int current = (int) document.get("count");
				// addCount(current);
				counter.addToCounter(current);
			}
		});

		return counter.getCount();
	}
	
	public void updateHeadcount(final HeadCount headcountUpdate){
	
		MongoCollection<Document> collection = db.getCollection(headcountUpdate.getKey_collection());
	
		FindIterable<Document> iterable = collection
				.find(new Document(headcountUpdate.getKey_deviceId(), headcountUpdate.getId()));
		
		if(iterable.first()==null){
			//create and insert document
			Document doc = createHeadcountDocument(headcountUpdate); 
			collection.insertOne(doc);
		}else{
			iterable.forEach(new Block<Document>() {
	    	//Document already exists and is modified
			    @Override
			    public void apply(final Document document) {
			    	db.getCollection(headcountUpdate.getKey_collection()).updateOne(document, new Document("$set", new Document(headcountUpdate.getKey_count(), headcountUpdate.getNrOfEmployees())));
			    }
		    });
		}
		
	}
	
	public void deleteHeadcount(String deviceId){
		
		MongoCollection<Document> collection = db.getCollection(HeadCount.key_collection);
		FindIterable<Document> iterable = collection
				.find(new Document(HeadCount.key_deviceId, deviceId));
		
		iterable.forEach(new Block<Document>() {
	    	//Document already exists and is modified
			    @Override
			    public void apply(final Document document) {
			    	db.getCollection(HeadCount.key_collection).deleteOne(document);
			    }
		    });
	}
	
	//Returns the document that can be inserted to the collection
	private Document createHeadcountDocument(HeadCount headcount){
		Document doc = new Document(headcount.getKey_location(), headcount.getLocation())
	               .append(headcount.getKey_date(), headcount.getCreatedDate())
	               .append(headcount.getKey_deviceId(), headcount.getId())
	               .append(headcount.getKey_count(), headcount.getNrOfEmployees());	
	
		return doc;
	}

//	private void addCount(int count) {
//		totalCountForDay += count;
//	}
	
}
