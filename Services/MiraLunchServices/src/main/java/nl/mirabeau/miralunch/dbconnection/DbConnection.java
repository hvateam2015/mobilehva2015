package nl.mirabeau.miralunch.dbconnection;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

public class DbConnection {
	private MongoClient mongoClient;
	private MongoDatabase db;

	

	public DbConnection() {
		startConnection();
	}

	private void startConnection() {
		try {
			// To connect to mongodb server
			MongoClientURI mongoClientUri = new MongoClientURI(DbConfiguration.DB_URL);
			mongoClient = new MongoClient(mongoClientUri);
			
			// Now connect to your databases
			db = mongoClient.getDatabase(DbConfiguration.DB_NAME);
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
	}

	public MongoDatabase getConnection() {
		return db;
	}
	
	public String capitalizeWord(String word){
		return word.substring(0, 1).toUpperCase() + word.substring(1);
	}
}