package com.example.alex.geofencing;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 9/23/2015.
 */
public class GeofenceTransitionsIntentService extends IntentService  {
    public static final String TAG = "Error: ";
    public static final String ACTIONENTER = "com.example.alex.geofencing_Enter";
    public static final String ACTIONDWELL = "com.example.alex.geofencing_Dwell";
    public static final String ACTIONEXIT = "com.example.alex.geofencing_Leave";
    private int locationCheck = 0;
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public GeofenceTransitionsIntentService(String name) {
        super(name);
    }
    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }

    protected void onHandleIntent(Intent intent) {

        /*
            * Checks if the received intent is equal to 'cancel'.
            * Updates the headcount when the user presses the button on the
            * notification.
         */
        if (intent.getAction() != null) {
            if (intent.getAction().equals("Cancel")) {
                Log.e("response", "Button clicked");
                /*
                    * ToDo:
                    * 1) Remove user from the headcount and removes the localstorage
                 */
                return;
            }
        }

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();
        /*
            * List of all current ACITVE geofences
         */
        List<Geofence> triggerList = geofencingEvent.getTriggeringGeofences();
        // 0 = Error
        // 1 = Hoorn
        // 2 = Amsterdam

        String[] triggerIds = new String[triggerList.size()];
        /*
            * Checks whether the location is either 1 or 2
         */
        for (int i = 0; i < triggerIds.length; i++) {
            String stringId = triggerList.get(i).getRequestId();
            if (stringId.equals("MIRALUNCH_GEOFENCE_LOCATION_HOORN")) {
                locationCheck = 1;
            } else if (stringId.equals("MIRALUNCH_GEOFENCE_LOCATION_AMSTERDAM")) {
                locationCheck = 2;
            } else {
                System.out.println("You are LOST!");
            }
        }


        // Test that the reported transition was of interest.
        /*if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            String transitionType = getTransitionString(geofenceTransition);*/
            //System.out.println("Transition type: " + transitionType);
            /*
                * Checks wether the user ENTERS or EXITS a geo-fence
             */
            if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
                switch (locationCheck) {
                    case 1:
                        System.out.println("Entering Hoorn");
                        break;
                    case 2:
                        System.out.println("Entering Amsterdam");
                        break;
                    default:
                        System.out.println("Entering unknown location.");
                        break;
                }

                /*
                    * Sends a notification to the broadcast receiver
                 */
                Intent intentReponse = new Intent(ACTIONENTER);
                intentReponse.putExtra("ENTERING", "Entering the area");
                LocalBroadcastManager.getInstance(this).sendBroadcast(intentReponse);
            }
            /*
                * Leaving an area

                else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
                    switch (locationCheck) {
                        case 1:
                            System.out.println("Leaving Hoorn");
                            break;
                        case 2:
                            System.out.println("Leaving Amsterdam");
                            break;
                        default:
                            System.out.println("Leaving unknown location.");
                            break;
                    }
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(this)
                                    .setContentTitle("Greetings")
                                    .setContentText("Exiting the are");


                    Intent intentReponse = new Intent(ACTIONEXIT);
                    intentReponse.putExtra("EXITING", "Exiting the area");
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intentReponse);
                }
            */
            // Get the geofences that were triggered. A single event can trigger
            // multiple geofences.

            /*
                List triggeringGeofences = geofencingEvent.getTriggeringGeofences();

                // Get the transition details as a String.
                String geofenceTransitionDetails = getGeofenceTransitionDetails(
                        this,
                        geofenceTransition,
                        triggeringGeofences
                );
            */

            // Send notification and log the transition details.
            //sendNotification(geofenceTransitionDetails);
            //Log.i(TAG, geofenceTransitionDetails);
        /*} else {
            // Log the error.
            Log.e(TAG, getString(R.string.geofence_transition_invalid_type,
                    geofenceTransition));
        }*/
    }
    private String getGeofenceTransitionDetails(
            Context context,
            int geofenceTransition,
            List<Geofence> triggeringGeofences) {

        String geofenceTransitionString = getTransitionString(geofenceTransition);

        // Get the Ids of each geofence that was triggered.
        ArrayList triggeringGeofencesIdsList = new ArrayList();
        for (Geofence geofence : triggeringGeofences) {
            triggeringGeofencesIdsList.add(geofence.getRequestId());
        }
        String triggeringGeofencesIdsString = TextUtils.join(", ", triggeringGeofencesIdsList);

        return geofenceTransitionString + ": " + triggeringGeofencesIdsString;
    }
    private String getTransitionString(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return getString(R.string.geofence_transition_entered);
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return getString(R.string.geofence_transition_exited);
            default:
                return getString(R.string.unknown_geofence_transition);
        }
    }
}