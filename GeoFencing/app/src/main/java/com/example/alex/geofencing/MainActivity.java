package com.example.alex.geofencing;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.location.Location;
import android.provider.SyncStateContract;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.ErrorDialogFragment;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.location.Geofence;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements /*MyReciever.TextChanger, */GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {
    //coords hoorn
    private static final double LONGITUDEHOORN = 5.047314;
    private static final double LATITUDEHOORN = 52.648311;

    private static final double latitudeTest = 56.944689;
    private static final double longitudeTest = 24.100986;

    private static final double LATITUDEAMSTERDAM = 52.335166;
    private static final double LONGITUDEAMSTERDAM = 4.928050;

//    private double longituteHoorn = 24.100986;
//    private double latitudeHoorn = 56.944689;
//
//    private double latitudeTest = 52.648311;
//    private double longitudeTest = 5.047314;

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    // Unique tag for the error dialog fragment
    private static final String DIALOG_ERROR = "dialog_error";
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;
    private static final String STATE_RESOLVING_ERROR = "resolving_error";
    private List<Geofence>  mGeofenceList = new ArrayList<>();
    private PendingIntent mGeofencePendingIntent;
    EditText tv = null;
    private static final String GEOFENCE_LOCATION_HOORN = "MIRALUNCH_GEOFENCE_LOCATION_HOORN";
    private static final String GEOFENCE_LOCATION_AMSTERDAM = "MIRALUNCH_GEOFENCE_LOCATION_AMSTERDAM";
    //public static final String action = "com.example.alex.geofencing";
    IntentFilter intentFilter = new IntentFilter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*
            * Connection to the Google Play Services
         */
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mResolvingError = savedInstanceState != null
                && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);

        /*
            * Adds conditions to the Geo-Fences
         */
        intentFilter.addAction(GeofenceTransitionsIntentService.ACTIONENTER);
        intentFilter.addAction(GeofenceTransitionsIntentService.ACTIONEXIT);

        /*
            * Sets up a new intent to IntentService and adds a trigger action
         */
        Intent cancelInt = new Intent(this, GeofenceTransitionsIntentService.class);
        cancelInt.setAction("Cancel");
        int requestId = (int) System.currentTimeMillis();
        /*
            * Creates a PendingIntent to listen to changes
         */
        PendingIntent cancelIntent = PendingIntent.getService(this, requestId, cancelInt,
                PendingIntent.FLAG_UPDATE_CURRENT |
                        PendingIntent.FLAG_CANCEL_CURRENT);

        /*
             * Sets up a new notification
             * .addAction adds a button that links to a service class.
             * ToDO:
             * 1) Create logo icon
             * 2) Proper Text
         */
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.common_signin_btn_icon_dark)
                        .setContentTitle("My notification")
                        .setContentText("Hello World!")
                        .addAction(R.drawable.common_signin_btn_text_normal_dark, "Ik ga toch niet!", cancelIntent);


        /*
            * Adds a new intent that links to the main activity class
         */
        Intent resultIntent = new Intent(this, MainActivity.class);


        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());


        tv = (EditText)findViewById(R.id.radius);

       // registerReceiver(broadcastReceiver, intentFilter);
    }
    /*
        * Recives a broadcast intent and edits the UI
     */
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            // goAsync()
            if(intent.getAction().equals(GeofenceTransitionsIntentService.ACTIONENTER)) {
                String message = intent.getStringExtra("ENTERING");
                tv.setText("In the zone");
            }
            if(intent.getAction().equals(GeofenceTransitionsIntentService.ACTIONEXIT)) {
                String message = intent.getStringExtra("EXITING");
                tv.setText("Out the zone");
            }
        }
    };

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);
        super.onResume();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
        * ToDo:
        * After implementing, delete the reset function
     */
    boolean swap = false;
    public void reset(View view){
        EditText et = (EditText)findViewById(R.id.editText);
        EditText et2 = (EditText)findViewById(R.id.editText2);

        if(!swap){
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                et.setText(String.valueOf(mLastLocation.getLatitude()));
                et2.setText(String.valueOf(mLastLocation.getLongitude()));
            }
            swap = true;
        } else {
            Location testLocation = new Location(mLastLocation);
            testLocation.setLatitude(latitudeTest);
            testLocation.setLongitude(longitudeTest);
            mLastLocation = testLocation;
            if (mLastLocation != null) {
                et.setText(String.valueOf(mLastLocation.getLatitude()));
                et2.setText(String.valueOf(mLastLocation.getLongitude()));
            }
            swap = false;
        }
    }
    /*
        * Adds a new fance. Coordinates with coordinates, a range and a given ID
        * ToDo:
        * Add a float parameter for the range
     */
    public void addFence(double longitude, double latitude, String id){
        mGeofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this
                // geofence.
                .setRequestId(id)

                .setCircularRegion(
                        latitude,
                        longitude,
                        1000f
                )
                .setExpirationDuration(60000)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());
        /*
            * Makes a call to the Google Play Services to add a Geo-Fence
         */
        LocationServices.GeofencingApi.addGeofences(
                mGoogleApiClient,
                getGeofencingRequest(),
                getGeofencePendingIntent()
        ).setResultCallback(this);


    }
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        System.out.println(builder.build().toString());
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.

        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent backmt when
        // calling addGeofences() and removeGeofences().
        //System.out.println("cookeieokei");
        return PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
    }



    /*
        * Connected to the play services
     */
    @Override
    public void onConnected(Bundle bundle) {
        EditText et = (EditText)findViewById(R.id.editText);
        EditText et2 = (EditText)findViewById(R.id.editText2);
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            et.setText(String.valueOf(mLastLocation.getLatitude()));
            et2.setText(String.valueOf(mLastLocation.getLongitude()));
        }

        addFence(LONGITUDEHOORN, LATITUDEHOORN, GEOFENCE_LOCATION_HOORN);
        addFence(LONGITUDEAMSTERDAM, LATITUDEAMSTERDAM, GEOFENCE_LOCATION_AMSTERDAM);
    }

    @Override
    public void onConnectionSuspended(int i) {
        // The connection has been interrupted.
        // Disable any UI components that depend on Google APIs
        // until onConnected() is called.
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // This callback is important for handling errors that
        // may occur while attempting to connect with Google.
        //
        // More about this in the 'Handle Connection Failures' section.
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GoogleApiAvailability.getErrorDialog()
            showErrorDialog(result.getErrorCode());
            mResolvingError = true;
        }

    }

    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getSupportFragmentManager(), "errordialog");
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_RESOLVING_ERROR, mResolvingError);
    }
    /*
        * Starts the connection to the Google Play Services
    */
    @Override
    protected void onStart() {
        super.onStart();
        if (!mResolvingError) {  // more about this later
            mGoogleApiClient.connect();

        }
    }
    /*
        * Stops the connection to the Google Play Services
    */
    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onResult(Status status) {

    }

//    @Override
//    public void setText(String text) {
//        tv.setText(text);
//    }


    /* A fragment to display an error dialog */
    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() { }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the error code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GoogleApiAvailability.getInstance().getErrorDialog(
                    this.getActivity(), errorCode, REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ((MainActivity) getActivity()).onDialogDismissed();
        }

        public void show(FragmentManager supportFragmentManager, String errordialog) {
            System.out.println(errordialog);

        }
    }
}

//class MyReciever extends BroadcastReceiver {
//
//    final TextChanger view;
//
//    MyReciever(TextChanger view) {
//        this.view = view;
//    }
//
//
//    @Override
//    public void onReceive(Context context, Intent intent) {
//        view.setText("jippie");
//    }
//
//    interface TextChanger {
//        void setText(String text);
//    }
//}