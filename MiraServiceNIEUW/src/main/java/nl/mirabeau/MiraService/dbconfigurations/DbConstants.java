package nl.mirabeau.MiraService.dbconfigurations;

public class DbConstants {
//	public static final String DB_URL = System.getenv("OPENSHIFT_MONGODB_DB_URL");
	public static final String DB_URL = "mongodb://localhost:27017";
	public static final String DB_USERNAME = System.getenv("OPENSHIFT_MONGODB_DB_USERNAME");
	public static final String DB_PASSWORD = System.getenv("OPENSHIFT_MONGODB_DB_PASSWORD");
	public static final String DB_NAME = "MiraLunch";
}
