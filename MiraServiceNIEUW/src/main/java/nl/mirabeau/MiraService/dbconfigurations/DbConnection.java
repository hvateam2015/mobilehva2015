package nl.mirabeau.MiraService.dbconfigurations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

public class DbConnection {
	private static final Logger logger = LoggerFactory.getLogger(DbConnection.class);
	private MongoClient mongoClient;
	private MongoDatabase db;

	public DbConnection() {
		startConnection();
	}

	private void startConnection() {
		try {
			// To connect to mongodb server
			MongoClientURI mongoClientUri = new MongoClientURI(DbConstants.DB_URL);
			mongoClient = new MongoClient(mongoClientUri);
			
			// Now connect to your databases
			db = mongoClient.getDatabase(DbConstants.DB_NAME);
			
			logger.debug("========= MONGO DATABASE DOET HET ==========");
		} catch (Exception e) {
			logger.debug("========= MONGO DATABASE DOET HET NIET ==========");
//			logger.equals(e.getClass().getName() + ": " e.getMessage());
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
	}

	public MongoDatabase getConnection() {
		return db;
	}
	
	public String capitalizeWord(String word){
		return word.substring(0, 1).toUpperCase() + word.substring(1);
	}
}