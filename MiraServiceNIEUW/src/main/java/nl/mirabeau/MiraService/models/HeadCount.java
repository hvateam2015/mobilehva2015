package nl.mirabeau.MiraService.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;

/**
 * @author David Kooijman
 *
 *HeadCount class - Create a new instance of a HeadCount object.
 */
public class HeadCount{
    
	private String id;
    private long createdDate;
    private String location;
    private int nrOfEmployees;
    private int nrOfGuests;
    
    public static String key_count = "count";
    private static String key_date = "date";
    public static String key_location = "location"; 
    public static String key_deviceId = "device_id";
    public static String key_collection = "headcount";
	    
     
    /**
     * @param location - String with location of the count
     * @param nrOfPersons - Total amount of persons that will come. 1 for the person itself plus the rest for guests
     */
    public HeadCount(String id, long createdDate, String location, int nrOfEmployees){
    	this.id = id;
    	this.createdDate = createdDate;
    	this.location = location;
    	this.nrOfEmployees = nrOfEmployees;
    }
    
public HeadCount(){}
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    @JsonSerialize(using=DateSerializer.class)
    public long getCreatedDate() {
        return createdDate;
    }
    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }
    
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public int getNrOfEmployees() {
        return nrOfEmployees;
    }
    public void setNrOfEmployees(int nrOfEmployees) {
        this.nrOfEmployees = nrOfEmployees;
    }
  
    public void setNrOfGuests(int nrOfGuests) {
        this.nrOfGuests = nrOfGuests;
    }
    public int getTotalHeadCount(){
    	return this.nrOfEmployees + this.nrOfGuests;
    }

	public String getKey_count() {
		return key_count;
	}

	public String getKey_date() {
		return key_date;
	}

	public String getKey_location() {
		return key_location;
	}

	public String getKey_deviceId() {
		return key_deviceId;
	}

	public String getKey_collection() {
		return key_collection;
	}
     

     
}