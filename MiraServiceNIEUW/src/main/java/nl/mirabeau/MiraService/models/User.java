package nl.mirabeau.MiraService.models;

public class User{
	public static final String KEY_USERNAME="username";
	public static final String KEY_PASSWORD="password";
	public static final String KEY_LOCATION="location";
	public static final String KEY_COLLECTION="admin";
	
	
	private String username;
	private String password;
	private String location;

    public User(String username,String password, String location){
    	this.username = username;
    	this.password = password;
    	this.location = location;
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
}