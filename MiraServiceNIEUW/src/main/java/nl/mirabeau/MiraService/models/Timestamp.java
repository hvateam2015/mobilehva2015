package nl.mirabeau.MiraService.models;

import java.util.Calendar;

public class Timestamp{
	
	   //returns the Timestamp in the right format.
	   public long getTimestamp(){
		    Calendar cal = Calendar.getInstance();
			
			cal.set(Calendar.MILLISECOND, 0);
			cal.set(Calendar.SECOND,  0);
			cal.set(Calendar.MINUTE,  0);
			cal.set(Calendar.HOUR, 0);
						
	        return cal.getTime().getTime();
	   }
	   
	
}