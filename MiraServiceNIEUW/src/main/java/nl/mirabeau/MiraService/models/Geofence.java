package nl.mirabeau.MiraService.models;

public class Geofence{
	public static final String GEOFENCE_COLLECTION = "geofence";
	public static final String GEOFENCE_LOCATION = "location";
	public static final String GEOFENCE_LONGITUDE = "longitude";
	public static final String GEOFENCE_LATITUDE = "latitude";
	public static final String GEOFENCE_RADIUS = "radius";
	
	private String location;
    private double longitude;
    private double latitude;
    private float radius;

	// Constructors
	public Geofence() {}

	public String getLocation() {
        return location;
    }
	
	public void setLocation(String location) {
		this.location = location;
	}

	public double getLongitude(){
		return longitude;
	}
	
	public void setLongitude(double longitude){
		this.longitude = longitude;
	}
	
	public double getLatitude(){
		return latitude;
	}
	
	public void setLatitude(double latitude){
		this.latitude = latitude;
	}
	
	public float getRadius(){
		return radius;
	}
	
	public void setRadius(double radius){
		this.radius = (float) radius;
	}

	@Override
	public String toString() {
		return "Location=" + location + ", longitude=" + longitude + ", latitude=" + latitude + ", radius=" + radius;
	}
}