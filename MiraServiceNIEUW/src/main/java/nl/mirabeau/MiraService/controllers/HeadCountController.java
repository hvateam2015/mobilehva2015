package nl.mirabeau.MiraService.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import nl.mirabeau.MiraService.constants.URIConstants;
import nl.mirabeau.MiraService.daos.HeadCountDAO;
import nl.mirabeau.MiraService.models.HeadCount;
import nl.mirabeau.MiraService.models.Timestamp;

/**
 * Controller for headcount. All requests are handled here.
 * 
 */
@RestController
@RequestMapping("/headcount")
public class HeadCountController {
	private HeadCountDAO headCountDao = new HeadCountDAO();

	/**
	 * @param location - the location of the headcount
	 * @return totalCount - Integer that returns the total headcount for a day and location.
	 */
	@RequestMapping(value = URIConstants.GET_ITEM_TODAY, method = RequestMethod.GET)
	public @ResponseBody int getDummyHeadCount(@PathVariable("location") String location) {
		Timestamp stamp = new Timestamp();

//		Calendar cal = Calendar.getInstance();
//
//		cal.set(Calendar.MILLISECOND, 0);
//		cal.set(Calendar.SECOND, 0);
//		cal.set(Calendar.MINUTE, 0);
//		cal.set(Calendar.HOUR, 0); ik weet niet of je dit nog wilt hebben


		// int totalCount = headCountDao.getHeadCount(cal.getTimeInMillis(),
		// location);
		int totalCount = headCountDao.getHeadCount(stamp.getTimestamp(), location);

		return totalCount;
	}
	

	@ResponseBody
	@RequestMapping(value = URIConstants.EDIT_OBJECT + "/update", method = RequestMethod.POST)
	public ResponseEntity<String> manipulateHeadcount(HttpServletRequest request, HttpServletResponse response) {

		
		HeadCount headcountObject = createHeadcountObject(request); 
		headCountDao.updateHeadcount(headcountObject); //puts a new document in the mongodb
				
		return new ResponseEntity<String>("Current headcount is modified.",
					HttpStatus.ACCEPTED);
		
	}
	
	@ResponseBody
	@RequestMapping(value = URIConstants.EDIT_OBJECT + "/changeLocation", method = RequestMethod.POST)
	public ResponseEntity<String> changeLocation(HttpServletRequest request, HttpServletResponse response) {

		
		HeadCount headcountObject = createHeadcountObject(request); 
		headCountDao.changeLocation(headcountObject); //puts a new document in the mongodb
				
		return new ResponseEntity<String>("Current location is modified.",
					HttpStatus.ACCEPTED);
		
	}
	
	@ResponseBody
	@RequestMapping(value = URIConstants.EDIT_OBJECT + "/delete", method = RequestMethod.POST)
	public ResponseEntity<String> deleteHeadcount(HttpServletRequest request, HttpServletResponse response) {

		headCountDao.deleteHeadcount(request.getParameter(HeadCount.key_deviceId)); 
		//deletes the document for a device-id
				
		return new ResponseEntity<String>("Your headcount is deleted.",
					HttpStatus.ACCEPTED);
		
	}
	
	
	   //returns a new HeadCount object. Which contains the values that needs to be stored in the MongoDB
	   private HeadCount createHeadcountObject(HttpServletRequest request){ 
		   Timestamp stamp = new Timestamp();
		   
	    	//get headcount in integer
			String count = request.getParameter(HeadCount.key_count);
			int headcountUpdate = Integer.parseInt(count);		 //headcount
			
			//get deviceid in integer
			String deviceId = request.getParameter(HeadCount.key_deviceId); //device-id

			String location = request.getParameter(HeadCount.key_location);  //location
			long timestamp = stamp.getTimestamp();					//date
	
			HeadCount headcountObject = new HeadCount(deviceId, timestamp , location, headcountUpdate);

			return headcountObject;
	    }
	   
	   
	
}
