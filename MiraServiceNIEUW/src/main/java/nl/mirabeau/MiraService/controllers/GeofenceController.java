package nl.mirabeau.MiraService.controllers;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import nl.mirabeau.MiraService.models.Geofence;
import nl.mirabeau.MiraService.constants.URIConstants;
import nl.mirabeau.MiraService.daos.GeofenceDAO;

/**
 * Handles requests For the Employee service.
 */
@RestController
@RequestMapping("/geofence")
public class GeofenceController {
	
	GeofenceDAO geofenceDAO = new GeofenceDAO();
	
	@RequestMapping(value = URIConstants.GET_ALL_GEOFENCES, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<Geofence>> getGeofence(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<Geofence> geofence = geofenceDAO.getAllGeofences();
		
		return new ResponseEntity<ArrayList<Geofence>>(geofence, HttpStatus.ACCEPTED);
	}
}