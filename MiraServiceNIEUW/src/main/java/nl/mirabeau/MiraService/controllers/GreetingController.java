package nl.mirabeau.MiraService.controllers;

import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import nl.mirabeau.MiraService.models.Greeting;

@Controller
public class GreetingController {
	private static final String template = "Hello %s!";
	private final AtomicLong counter = new AtomicLong();

	@RequestMapping(value = "/greeting", method = RequestMethod.GET)
	public @ResponseBody Greeting getGreeting(@RequestParam(required = false, defaultValue = "World") String name) {
		System.out.println("==== in greeting ====");
		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}

	@RequestMapping(value = "/greeting", method = RequestMethod.POST)
	public @ResponseBody Greeting postGreeting(HttpServletRequest request, HttpServletResponse response) {

		String name = request.getParameter("dishMa");
		name += ", " + request.getParameter("dishDi");
		name += ", " + request.getParameter("dishWo");
		name += ", " + request.getParameter("dishDo");
		name += ", " + request.getParameter("dishFr");
		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}
}
