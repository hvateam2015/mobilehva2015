package nl.mirabeau.MiraService.controllers;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import nl.mirabeau.MiraService.daos.UserDAO;
import nl.mirabeau.MiraService.models.User;

/**
 * Controller for admin users. All requests are handled here.
 * 
 */

@RestController
@RequestMapping("/user")
public class UserController {
	private UserDAO userDao = new UserDAO();

	@ResponseBody
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<String> checkCredentials(HttpServletRequest request, HttpServletResponse response) {

		// request parameters
		String username = request.getParameter(User.KEY_USERNAME);
		String password = request.getParameter(User.KEY_PASSWORD);
				 
		String authentication = userDao.authenticateUser(username, password); 
		
		if(authentication != null){
			return new ResponseEntity<String>(authentication.toString(), HttpStatus.ACCEPTED);
		}else{
			return new ResponseEntity<String>("Validation failed.", HttpStatus.UNAUTHORIZED);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/generate-password/{plainPassword}", method = RequestMethod.GET)
	public ResponseEntity<String> generatePassword(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("plainPassword") String plainPassword) {
		String hash = userDao.getHash(plainPassword);
		return new ResponseEntity<String>(hash, HttpStatus.ACCEPTED);
	}
}