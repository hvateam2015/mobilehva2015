package nl.mirabeau.MiraService.controllers;

import java.util.Calendar;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import nl.mirabeau.MiraService.constants.URIConstants;

/**
 * Handles requests For the Time service.
 */
@RestController
@RequestMapping("/time")
public class TimeController {

	/**
	 * The current time in milliseconds. This prevents the time being spoofed in an app.
	 * 
	 * @return cal - the time in milliseconds
	 */
	@RequestMapping(value = URIConstants.CHECK_TIME, method = RequestMethod.GET)
	public @ResponseBody long getCurrentTime() {
		Calendar cal = Calendar.getInstance();
		
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR, 0);
		
		return cal.getTimeInMillis();
	}
}
