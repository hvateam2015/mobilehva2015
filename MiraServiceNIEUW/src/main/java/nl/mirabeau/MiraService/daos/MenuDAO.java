package nl.mirabeau.MiraService.daos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;

import nl.mirabeau.MiraService.dbconfigurations.DbConnection;
import nl.mirabeau.MiraService.models.Day;
import nl.mirabeau.MiraService.models.Week;

public class MenuDAO {
	private MongoDatabase db;
	private DbConnection con;
	private HeadCountDAO headCountDao = new HeadCountDAO();
	private Week week;
	private Day[] days;

	public MenuDAO() {
		con = new DbConnection();
		db = con.getConnection();
		week = new Week();
		days = new Day[5];
		
	}

	public Week getMenu(String location, int weekNr, int year) {
		week = new Week();
		
		FindIterable<Document> iterable = db.getCollection(Week.WEEK_COLLECTION)
				.find(new Document(Week.WEEK_NR, weekNr).append(Week.WEEK_LOCATION, con.capitalizeWord(location)).append(Week.WEEK_YEAR,  year));
		
		iterable.forEach(new Block<Document>() {
		    @Override
		    public void apply(final Document document) {
		    	week.setYear(document.getInteger(Week.WEEK_YEAR));
		    	week.setLocation(document.getString(Week.WEEK_LOCATION));
		    	week.setWeekNr(document.getInteger(Week.WEEK_NR));
		    	
		    	@SuppressWarnings("unchecked")
				ArrayList<Document> dishes = (ArrayList<Document>) document.get(Week.WEEK_DAYS);
		    	for(int i = 0; i < dishes.size(); i++){
		    		long timestamp = dishes.get(i).getLong(Day.DAY_DATE);
		    		days[i] = new Day(timestamp, dishes.get(i).getString(Day.DAY_DISH) );
		    		days[i].setHeadCount(headCountDao.getHeadCount(timestamp, week.getLocation()));
		    	}
		    	
		    	week.setDays(days);
		    }
		});
		
		return week;
	}
	
	public Week createMenu(String location, int weekNr, int year){
		week = new Week();
		
		String defaultDish = "Vul hier uw gerecht in";
		
		location = con.capitalizeWord(location);
		
		Day[] listOfDays  = new Day[5];
		Document[] documentDays = new Document[5];
		
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.WEEK_OF_YEAR, weekNr);
		
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND,  0);
		cal.set(Calendar.MINUTE,  0);
		cal.set(Calendar.HOUR, 0);

		long dateTime = cal.getTime().getTime();
		
		for(int i = 0; i < listOfDays.length; i++){
			switch (i){
				case 0:
					cal.set(Calendar.DAY_OF_WEEK,  Calendar.MONDAY);
					dateTime = cal.getTime().getTime();
					listOfDays[i] = new Day(dateTime, defaultDish);
					documentDays[i] = new Document().append(Day.DAY_DATE , dateTime).append(Day.DAY_DISH , defaultDish);
					break;
				case 1:
					cal.set(Calendar.DAY_OF_WEEK,  Calendar.TUESDAY);
					dateTime = cal.getTime().getTime();
					listOfDays[i] = new Day(dateTime, defaultDish);
					documentDays[i] = new Document().append(Day.DAY_DATE , dateTime).append(Day.DAY_DISH , defaultDish);
					break;
				case 2: 
					cal.set(Calendar.DAY_OF_WEEK,  Calendar.WEDNESDAY);
					dateTime = cal.getTime().getTime();
					listOfDays[i] = new Day(dateTime, defaultDish);
					documentDays[i] = new Document().append(Day.DAY_DATE , dateTime).append(Day.DAY_DISH , defaultDish);
					break;
				case 3:
					cal.set(Calendar.DAY_OF_WEEK,  Calendar.THURSDAY);
					dateTime = cal.getTime().getTime();
					listOfDays[i] = new Day(dateTime, defaultDish);
					documentDays[i] = new Document().append(Day.DAY_DATE , dateTime).append(Day.DAY_DISH , defaultDish);
					break;
				case 4: 
					cal.set(Calendar.DAY_OF_WEEK,  Calendar.FRIDAY);
					dateTime = cal.getTime().getTime();
					listOfDays[i] = new Day(dateTime, defaultDish);
					documentDays[i] = new Document().append(Day.DAY_DATE , dateTime).append(Day.DAY_DISH , defaultDish);
					break;
				default:
					break;
			}
		}
		
		week.setYear(year);
		week.setLocation(location);
		week.setWeekNr(weekNr);
		week.setDays(listOfDays);
		
		// save week in database.
		db.getCollection(Week.WEEK_COLLECTION).insertOne(
		        new Document()
		        		.append(Week.WEEK_LOCATION, location)
		                .append(Week.WEEK_NR, weekNr)
		                .append(Week.WEEK_YEAR, year)
		                .append(Week.WEEK_DAYS, Arrays.asList(documentDays))
		                );
		        
		return week;
	}
	
	public void updateWeek(Week week){
		Document[] documentDays = new Document[5];
		Day[] listOfDays = week.getDays();
		
		for(int i = 0; i < listOfDays.length; i++){
			documentDays[i] = new Document().append(Day.DAY_DISH, listOfDays[i].getDish()).append(Day.DAY_DATE, listOfDays[i].getDate().getTime());
		}
		
		db.getCollection(Week.WEEK_COLLECTION).updateOne(new Document().append(Week.WEEK_NR, week.getWeekNr()).append(Week.WEEK_YEAR, week.getYear()).append(Week.WEEK_LOCATION, week.getLocation()),
		        new Document("$set", new Document(Week.WEEK_DAYS, Arrays.asList(documentDays))));
	}
}
