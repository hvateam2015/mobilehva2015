package nl.mirabeau.MiraService.daos;

import java.util.ArrayList;

import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;

import nl.mirabeau.MiraService.dbconfigurations.DbConnection;
import nl.mirabeau.MiraService.models.Geofence;

public class GeofenceDAO {
	private MongoDatabase db;
	private DbConnection con;
	private ArrayList<Geofence> geofences;


	public GeofenceDAO() {
		con = new DbConnection();
		db = con.getConnection();
		geofences = new ArrayList<Geofence>();
	}
	
	public ArrayList<Geofence> getAllGeofences(){
		geofences = new ArrayList<Geofence>();
		FindIterable<Document> iterable = db.getCollection(Geofence.GEOFENCE_COLLECTION).find();
		iterable.forEach(new Block<Document>(){
			@Override
			public void apply(final Document document){
				Geofence geofence = new Geofence();
				geofence.setLocation(document.getString(Geofence.GEOFENCE_LOCATION));
				geofence.setLongitude(document.getDouble(Geofence.GEOFENCE_LONGITUDE));
				geofence.setLatitude(document.getDouble(Geofence.GEOFENCE_LATITUDE));
				geofence.setRadius(document.getDouble(Geofence.GEOFENCE_RADIUS));
				geofences.add(geofence);
			}
		});
		
		return geofences;
	}
}