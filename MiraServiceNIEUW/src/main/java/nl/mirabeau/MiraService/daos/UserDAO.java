package nl.mirabeau.MiraService.daos;

import org.bson.Document;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.*;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import nl.mirabeau.MiraService.models.User;
import nl.mirabeau.MiraService.dbconfigurations.DbConnection;

public class UserDAO {
	private MongoDatabase db;
	private DbConnection con;

	public UserDAO() {
		con = new DbConnection();
		db = con.getConnection();
	}

	public String authenticateUser(String username, String password) {
		Document doc = db.getCollection(User.KEY_COLLECTION).find(eq(User.KEY_USERNAME, username)).first();

		if (doc == null) {
			return null;
		} else {
			if (password.equals(doc.get(User.KEY_PASSWORD)))
				return doc.getString(User.KEY_LOCATION).toString();
			else
				return null;
		}
	}

	public String getHash(String password) {

		MessageDigest md;

		try {

			md = MessageDigest.getInstance("SHA-512");

			md.update(password.getBytes());
			byte[] byteData = md.digest();

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}

			return sb.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return "something went wrong";
	}
}