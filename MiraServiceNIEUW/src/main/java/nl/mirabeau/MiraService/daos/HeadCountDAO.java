package nl.mirabeau.MiraService.daos;

import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import nl.mirabeau.MiraService.models.Counter;
import nl.mirabeau.MiraService.models.HeadCount;
import nl.mirabeau.MiraService.models.Timestamp;
import nl.mirabeau.MiraService.dbconfigurations.DbConnection;

public class HeadCountDAO {
	private MongoDatabase db;
	private DbConnection con;
	private Counter counter;

	public HeadCountDAO() {
		con = new DbConnection();
		db = con.getConnection();
	}

	public int getHeadCount(long dayTimestamp, String location) {
		counter = new Counter();

		FindIterable<Document> iterable = db.getCollection("headcount")
				.find(new Document("date", dayTimestamp).append("location", con.capitalizeWord(location)));

		iterable.forEach(new Block<Document>() {
			@Override
			public void apply(final Document document) {
				if (document.containsKey("count")) {
					int current = Integer.parseInt(document.get("count").toString());

					counter.addToCounter(current);
				}
			}
		});

		return counter.getCount();
	}

	
	public void updateHeadcount(final HeadCount headcountUpdate){
		
		Timestamp timestamp = new Timestamp();
		MongoCollection<Document> collection = db.getCollection(headcountUpdate.getKey_collection());

		FindIterable<Document> iterable = collection
				.find(new Document(headcountUpdate.getKey_deviceId(), headcountUpdate.getId())
						.append(headcountUpdate.getKey_date(),timestamp.getTimestamp()));
		
		if(iterable.first()==null){
			//create and insert document
			Document doc = createHeadcountDocument(headcountUpdate); 
			collection.insertOne(doc);
		} else {
			iterable.forEach(new Block<Document>() {
				// Document already exists and is modified
				@Override
				public void apply(final Document document) {
					db.getCollection(headcountUpdate.getKey_collection()).updateOne(document, new Document("$set",
							new Document(headcountUpdate.getKey_count(), headcountUpdate.getNrOfEmployees())));
				}
			});
		}

	}
	
	public void changeLocation(final HeadCount headcountObj) {
		
		Timestamp timestamp = new Timestamp();
		MongoCollection<Document> collection = db.getCollection(headcountObj.getKey_collection());

		FindIterable<Document> iterable = collection
				.find(new Document(headcountObj.getKey_deviceId(), headcountObj.getId())
						.append(headcountObj.getKey_date(),timestamp.getTimestamp()));

		if (iterable.first() == null) {
			// create and insert document
			Document doc = createHeadcountDocument(headcountObj);
			collection.insertOne(doc);
		} else {
			iterable.forEach(new Block<Document>() {
				// Document already exists and is modified
				@Override
				public void apply(final Document document) {
					db.getCollection(headcountObj.getKey_collection()).updateOne(document, new Document("$set",
							new Document(headcountObj.getKey_location(), headcountObj.getLocation())));
				}
			});
		}

	}

	public void deleteHeadcount(String deviceId) {

		MongoCollection<Document> collection = db.getCollection(HeadCount.key_collection);
		FindIterable<Document> iterable = collection.find(new Document(HeadCount.key_deviceId, deviceId));

		iterable.forEach(new Block<Document>() {
			// Document already exists and is modified
			@Override
			public void apply(final Document document) {
				db.getCollection(HeadCount.key_collection).deleteOne(document);
			}
		});
	}

	// Returns the document that can be inserted to the collection
	private Document createHeadcountDocument(HeadCount headcount) {
		Document doc = new Document(headcount.getKey_location(), headcount.getLocation())
				.append(headcount.getKey_date(), headcount.getCreatedDate())
				.append(headcount.getKey_deviceId(), headcount.getId())
				.append(headcount.getKey_count(), headcount.getNrOfEmployees());

		return doc;
	}
}
