package mirabeau.miralunch.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import mirabeau.miralunch.R;
import mirabeau.miralunch.activities.ScreenSlideActivity;
import mirabeau.miralunch.fragments.MenuCardFragment;
import mirabeau.miralunch.models.Location;
import mirabeau.miralunch.requests.HeadcountRequest;
import mirabeau.miralunch.utils.PermissionDetector;
import mirabeau.miralunch.utils.Urls;


public class GeofenceTransitionsIntentService extends IntentService {
    public static final String TAG = "Error: ";
    public static final String ACTIONENTER = "Mirabeau.miralunch.geofencing_Enter";
    public static final String ACTIONEXIT = "Mirabeau.miralunch.geofencing_Leave";
    public static final String CHANGEPREFERENCES = "Mirabeau.miralunch.geofencing_Change";
    public static MenuCardFragment menuFragment;
    public ScreenSlideActivity screenSlideActivity;
    private SharedPreferences sp;
    private PendingIntent cancelIntent;
    private NotificationCompat.Builder  mBuilder;
    private String[] triggerIds;
    private List<Geofence> triggerList;
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public GeofenceTransitionsIntentService(String name) {
        super(name);
    }
    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }

    protected void onHandleIntent(Intent intent) {
        // Get shared preference manager
        sp = getApplicationContext().getSharedPreferences(getString(R.string.local_storage_appName), Context.MODE_PRIVATE);

        if (intent.getAction() != null && intent.getAction().equals("Cancel")) {
            //checks if the cancel button of the notification is clicked
            handlecancelNotification(intent);
        } else {

            // Read the geofencingEvents from the incoming intent.
            GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
            int geofenceTransition = geofencingEvent.getGeofenceTransition();
            triggerList = geofencingEvent.getTriggeringGeofences();
            triggerIds = new String[triggerList.size()];

            // Check for settings init and attendance-y.
            if (!signInCheckInit() && signInCheckSettings() && !signInCheckMainScreen()) {
            /*
             * Sets up a new notification
             * .addAction adds a button that sends a 'Cancel' intent.
            */
                createCancelNotification();

                //Adds a new intent that links to the main activity class
                Intent resultIntent = new Intent(this, ScreenSlideActivity.class);
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                stackBuilder.addParentStack(ScreenSlideActivity.class);
                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                sendNotification();
                mBuilder.setContentIntent(resultPendingIntent);
                sendBroadcast(geofenceTransition);
            }
        }
    }
    public void createCancelNotification(){
        //Sets up a new intent to IntentService and adds a trigger action
        Intent cancelInt = new Intent(this, GeofenceTransitionsIntentService.class);
        cancelInt.setAction("Cancel");
        int requestId = (int) System.currentTimeMillis();

        //Creates a PendingIntent to listen to changes
        PendingIntent cancelIntent = PendingIntent.getService(this, requestId, cancelInt,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_CANCEL_CURRENT);

        mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.common_signin_btn_icon_dark)
                .setContentTitle("Lunch tijd")
                .addAction(R.drawable.common_signin_btn_text_normal_dark, "Ik ga toch niet!", cancelIntent);
    }

    public void sendNotification(){
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //loops through the list of geofences. If the id matches, change the notification text
        //mNotificationManager.notify sends the notification to the user

        Log.e("TriggerIds", triggerIds.length+"");
        for (int i = 0; i < triggerIds.length; i++) {
            Location loc = new Location(getApplicationContext());
            String stringId = triggerList.get(i).getRequestId();
            if (stringId.equals(getString(R.string.geofence_hoorn))) {
                mBuilder.setContentText("Lunch tijd in Hoorn");
                mNotificationManager.notify(1, mBuilder.build());
                loc.setCurrentLocation("Hoorn");
            } else if (stringId.equals(getString(R.string.geofence_amsterdam))) {
                mBuilder.setContentText("Lunch tijd in Amsterdam");
                mNotificationManager.notify(1, mBuilder.build());
                loc.setCurrentLocation("Amsterdam");
            } else if (stringId.equals(getString(R.string.geofence_nijmegen))) {
                mBuilder.setContentText("Lunch tijd in Nijmegen");
                mNotificationManager.notify(1, mBuilder.build());
                loc.setCurrentLocation("Nijmegen");
            } else if (stringId.equals(getString(R.string.geofence_leeuwarden))) {
                mBuilder.setContentText("Lunch tijd in Leeuwarden");
                mNotificationManager.notify(1, mBuilder.build());
                loc.setCurrentLocation("Leeuwarden");
            } else if (stringId.equals(getString(R.string.geofence_rotterdam))) {
                mBuilder.setContentText("Lunch tijd in Rotterdam");
                mNotificationManager.notify(1, mBuilder.build());
                loc.setCurrentLocation("Rotterdam");
            }
        }
        Location loc = new Location(getApplicationContext());
        Log.e("Location has been set", loc.getCurrentLocation());
    }
    public void sendBroadcast(int geofenceTransition){
        //Checks if the Transition is an ENTER transition
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            //Sends a notification to the broadcast receiver
            Intent intentReponse = new Intent(ACTIONENTER);
            intentReponse.putExtra("ENTERING", "Entering the area");
            LocalBroadcastManager.getInstance(this).sendBroadcast(intentReponse);
            //changes the button on the main screen
            SharedPreferences.Editor spEditor = sp.edit();
            spEditor.putBoolean(getString(R.string.local_storage_present), true);
            spEditor.apply();
            HeadcountRequest.createRequest(getApplicationContext(), Urls.getSetHeadcount());
            //edits the shared preferences
            Intent editSignUp = new Intent(CHANGEPREFERENCES);
            LocalBroadcastManager.getInstance(this).sendBroadcast(editSignUp);
        }
    }
    /*
     * Method called when the user clicks the cancel button on the notification
     */
    public void handlecancelNotification(Intent intent){
        //noinspection ResourceType
        new AsyncPOST() {
            @Override
            protected void onPostExecute(Void e) {
                super.onPostExecute(e);
                Log.i("ONPOSTEXECUTE", "setGuestHeadCountAsync in Gefofenccetransitionsintentservice");
            }
        }.execute(HeadcountRequest.createRequest(getApplicationContext(),Urls.getSetHeadcount()));
        return;
    }
    /*
     *@return boolean = true if the user already had a notification
     */
    public boolean signInCheckInit(){
        SharedPreferences.Editor spEditor = sp.edit();
        Calendar currentDate = Calendar.getInstance();
        //default date that is 1 day in the future
        Date defaultDate = new Date((Calendar.getInstance().getTimeInMillis()+86400000));
        Long present = sp.getLong(getString(R.string.local_storage_initial_check), defaultDate.getTime());
        //create new shared preferences date
        Calendar sharedPreferencesDate = Calendar.getInstance();
        Date date = new Date(present);

        sharedPreferencesDate.setTime(date);
        sharedPreferencesDate.set(Calendar.HOUR_OF_DAY, 0);
        sharedPreferencesDate.set(Calendar.MINUTE, 0);
        sharedPreferencesDate.set(Calendar.SECOND, 0);
        sharedPreferencesDate.set(Calendar.MILLISECOND, 0);

        currentDate.set(Calendar.HOUR_OF_DAY, 0);
        currentDate.set(Calendar.MINUTE, 0);
        currentDate.set(Calendar.SECOND, 0);
        currentDate.set(Calendar.MILLISECOND, 0);

        if(sharedPreferencesDate.compareTo(currentDate) != 0){
            //dates are NOT equal - replace date with date of today
            Log.e("dates", "Dates aren't equal");
            spEditor.putLong(getString(R.string.local_storage_initial_check), date.getTime());
            return false;
        }
        return true;
    }
    /*
     *@return boolean = true if today is set in the shared preferences
     */
    public boolean signInCheckSettings(){
        Calendar c = Calendar.getInstance();
        //Don't do the check if it's saturday(7) or sunday(1)
        if(c.get(c.DAY_OF_WEEK) == 7 || c.get(c.DAY_OF_WEEK) == 1){
            return false;
        }

        //list of shared preferences settings
        Set<String> presentList = sp.getStringSet(getString(R.string.settings_days_key), null);
        String[] stringList = presentList.toArray(new String[]{});

        String day = convertToCalendarDay(c.get(Calendar.DAY_OF_WEEK));
        //loops through the settings list to find a match
        for (String s : stringList) {
            if (day.equals(s)) {
                return true;
            }
        }
        return false;
    }
    /*
     * @return boolean = true if the user is checked in for the lunch or not at the main screen
     */
    public boolean signInCheckMainScreen() {
        return sp.getBoolean(getString(R.string.local_storage_present), false);
    }
    /*
     *@return String = conversion from index to string for comparison
     */
    private String convertToCalendarDay(int day){
        String[] sharedPrefsWorkDays = getResources().getStringArray(R.array.workdays_storage);
         switch (day) {
            case 2:
                return sharedPrefsWorkDays[0];
            case 3:
                return sharedPrefsWorkDays[1];
            case 4:
                return sharedPrefsWorkDays[2];
            case 5:
                return sharedPrefsWorkDays[3];
            case 6:
                return sharedPrefsWorkDays[4];
            default:
                //Weekend
                return null;
        }

    }

    private String getGeofenceTransitionDetails(
            Context context,
            int geofenceTransition,
            List<Geofence> triggeringGeofences) {

        String geofenceTransitionString = getTransitionString(geofenceTransition);

        // Get the Ids of each geofence that was triggered.
        ArrayList triggeringGeofencesIdsList = new ArrayList();
        for (Geofence geofence : triggeringGeofences) {
            triggeringGeofencesIdsList.add(geofence.getRequestId());
        }
        String triggeringGeofencesIdsString = TextUtils.join(", ", triggeringGeofencesIdsList);

        return geofenceTransitionString + ": " + triggeringGeofencesIdsString;
    }
    private String getTransitionString(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return getString(R.string.geofence_transition_entered);
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return getString(R.string.geofence_transition_exited);
            default:
                return getString(R.string.geofence_transition_unknown);
        }
    }
}
