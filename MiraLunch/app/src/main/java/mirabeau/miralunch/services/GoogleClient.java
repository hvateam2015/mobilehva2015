package mirabeau.miralunch.services;

import android.annotation.SuppressLint;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;

import mirabeau.miralunch.activities.ScreenSlideActivity;


@SuppressWarnings("SpellCheckingInspection")
public class GoogleClient extends ScreenSlideActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {
    public GoogleApiClient mGoogleApiClient;

    private MiralunchGeofence miralunchGeofence;
    private boolean mResolvingError = false;
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    public ScreenSlideActivity activity;

    public GoogleClient(){}

    public GoogleClient(ScreenSlideActivity activity) {
        this.activity = activity;
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        miralunchGeofence = new MiralunchGeofence(activity, this);
    }


    public GoogleApiClient getApiClient(){
        return mGoogleApiClient;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mResolvingError) {
            mGoogleApiClient.connect();

        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    /*
        * Connected to the play services
     */
    @SuppressLint("NewApi")
    @Override
    public void onConnected(Bundle bundle) {
        Log.i("Connected", "connection has been established");
        if (!activity.hasAccessLocation()) {
            activity.requestPermissions(activity.getPermissionList(), 8008135);
            return;
        }

        miralunchGeofence.addFence();
    }

    @Override
    public void onConnectionSuspended(int i) {
        // The connection has been interrupted.
        // Disable any UI components that depend on Google APIs
        // until onConnected() is called.
    }

    @Override
    public void onResult(Status status) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // This callback is important for handling errors that
        // may occur while attempting to connect with Google.
        //
        // More about this in the 'Handle Connection Failures' section.
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        }
        if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GoogleApiAvailability.getErrorDialog()
            //showErrorDialog(result.getErrorCode());
            mResolvingError = true;
        }

    }


    public boolean isConnecting(){
        return mGoogleApiClient.isConnecting();
    }
    public boolean isConnected(){
        return mGoogleApiClient.isConnected();
    }
    public void connect(){
        System.out.println("Connecting...");
        mGoogleApiClient.connect();
    }
    @SuppressWarnings("unused")
    public void disconnect(){
        mGoogleApiClient.disconnect();
    }

}
