package mirabeau.miralunch.services;

import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class AsyncPOST extends AsyncTask<Request, Void, Void> {

   @Override
   protected Void doInBackground(Request... request) {
       
       OkHttpClient client = new OkHttpClient();

       try {
           Response response = client.newCall(request[0]).execute();
           Log.i("RESPONSE", response.body().string());
       } catch (IOException e) {
           e.printStackTrace();
           Log.i("RESPONSE", "Something went wrong in the headcount async task.");

       }

       return null;
   }


    protected void onPostExecute(Void e){}


}
