package mirabeau.miralunch.services;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mirabeau.miralunch.R;
import mirabeau.miralunch.activities.ScreenSlideActivity;
import mirabeau.miralunch.utils.PermissionDetector;
import mirabeau.miralunch.utils.Urls;


@SuppressWarnings({"SpellCheckingInspection", "unused"})
public class MiralunchGeofence extends GoogleClient {
    private List<Geofence> mGeofenceList = new ArrayList<>();
    private PendingIntent mGeofencePendingIntent;
    private ScreenSlideActivity activity;
    private PermissionDetector mPD;

    private GoogleClient googleClient;


    public MiralunchGeofence() {}

    public MiralunchGeofence(ScreenSlideActivity activity, GoogleClient googleClient) {
        this.activity = activity;
        this.googleClient = googleClient;
        mPD = new PermissionDetector(activity.getApplicationContext());
    }

    /*
        * Adds a new fance. Coordinates with coordinates, a range and a given ID
        * ToDo:
        * Add a float parameter for the range
     */
    public void addFence() {
        Log.i("Adding Fences", "application is adding the geoFences");
        if (activity.hasAccessLocation() && mPD.isNetworkLocationAvailable() && mPD.isGPSAvailable()) {
            new AsyncGET() {
                @Override
                protected void onPostExecute(JSONObject jsonObject) {
                    try {
                        JSONArray jsonArray = jsonObject.getJSONArray("main");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            String name = activity.getString(R.string.geofence_prefix);
                            name = name + jsonArray.getJSONObject(i).getString("location").toUpperCase();
                            double longitude = jsonArray.getJSONObject(i).getDouble("longitude");
                            double latitude = jsonArray.getJSONObject(i).getDouble("latitude");
                            float radius = (float) jsonArray.getJSONObject(i).getDouble("radius");

                            mGeofenceList.add(new Geofence.Builder()
                                    // Set the request ID of the geofence. This is a string to identify this
                                    // geofence.
                                    .setRequestId(name)

                                    .setCircularRegion(
                                            latitude,
                                            longitude,
                                            radius
                                    )
                                    .setExpirationDuration(60000)
                                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                                    .build());
                        }
                        LocationServices.GeofencingApi.addGeofences(
                                googleClient.getApiClient(),
                                getGeofencingRequest(),
                                getGeofencePendingIntent()
                        ).setResultCallback(googleClient);


                    } catch (JSONException e) {
                        Log.e("GEOFENCE FETCH", e.toString());
                    }
                }
            }.execute(Urls.getGeofences());
        } else {
            Log.e("MiralunchGeofence", "No Access to Location");
        }
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.

        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(activity, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent backmt when
        // calling addGeofences() and removeGeofences().
        return PendingIntent.getService(activity, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 8008135: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    addFence();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Log.e("Error", "NO permission");
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
            }
        }
    }
}
