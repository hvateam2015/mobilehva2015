package mirabeau.miralunch.services;

import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public abstract class AsyncGET extends AsyncTask<String, Void, JSONObject> {
    public AsyncGET(){}

    @Override
    protected JSONObject doInBackground(String... params) {
        String url_service = params[0];//url of the GET request

        Log.i("Processing async", "Processing async request");
        URL url;
        JSONObject jsonObject = new JSONObject();

        try{
            url = new URL(url_service);
            String line;

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            BufferedInputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder total = new StringBuilder();
            //Reads/appends the response
            while ((line = r.readLine()) != null) {
                Log.i("line", line);
                total.append(line);
            }

            try{
                JSONArray jsonArray = new JSONArray(total.toString());
                jsonObject.put("main", jsonArray);
            } catch (JSONException e){
                jsonObject = new JSONObject(total.toString());
            }

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
