package mirabeau.miralunch.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.squareup.okhttp.Request;

import mirabeau.miralunch.R;
import mirabeau.miralunch.requests.HeadcountRequest;
import mirabeau.miralunch.services.AsyncPOST;
import mirabeau.miralunch.utils.Urls;

public class Headcount {

    public int getHeadcount(Context context){

        return getNumberOfGuestsFromPref(context) + checkIfParticipating(context);
    }

    //checks if user is signed in for today's lunch.
    public int checkIfParticipating(Context context){
        //check Database function
        boolean present = context.getSharedPreferences(context.getString(R.string.local_storage_appName),
                Context.MODE_PRIVATE).getBoolean(context.getString(R.string.local_storage_present), false);

        if(present) return 1;
        else return 0;
    }

    public int getNumberOfGuestsFromPref(Context context){
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.local_storage_appName), Context.MODE_PRIVATE);
        int numberOfGuests = preferences.getInt(context.getString(R.string.local_storage_guest), -1);
        if (numberOfGuests == -1) {
            // the key does not exist
            Log.i("GET_PREFERENCES_GUEST", "number_of_guests does not exist");
            return 0;
        } else {
            Log.i("GET_PREFERENCES_GUEST", "number_of_guests: " + numberOfGuests);
            return numberOfGuests;
            // handle the value
        }
    }

    public void setNumberOfGuestsFromPref(Context context, int number_of_guests){
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.local_storage_appName), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Log.i("Number_of_guests", "" + number_of_guests);
        editor.putInt(context.getString(R.string.local_storage_guest), number_of_guests);
        editor.apply();
    }

    //This is does the changes in the service
    public void updateHeadcountInService(final Context context, final String toastMessage){

        Request request = HeadcountRequest.createRequest(context, Urls.getSetHeadcount());

        new AsyncPOST() {
            @Override
            protected void onPostExecute(Void e) {
                super.onPostExecute(e);
                Toast toast = Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT);
                toast.show();
            }
        }.execute(request);

    }

    //This is does the changes in the service
    public void changeLocationInService(final Context context){

        Request request = HeadcountRequest.createRequest(context, Urls.getLocationUpdate());

        new AsyncPOST() {
            @Override
            protected void onPostExecute(Void e) {
                super.onPostExecute(e);
                Toast toast = Toast.makeText(context, R.string.toast_changelocation, Toast.LENGTH_SHORT);
                toast.show();
            }
        }.execute(request);

    }

    //deletes the headcount by removing the document
    public void deleteHeadcountInService(final Context context){

        Request request = HeadcountRequest.createRequest(context, Urls.getDeleteHeadcount());

        new AsyncPOST() {
            @Override
            protected void onPostExecute(Void e) {
                super.onPostExecute(e);
                Toast toast = Toast.makeText(context, R.string.toast_count_deleted, Toast.LENGTH_SHORT);
                toast.show();
            }
        }.execute(request);

    }

}
