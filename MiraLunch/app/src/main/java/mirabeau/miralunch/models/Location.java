package mirabeau.miralunch.models;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import mirabeau.miralunch.R;


public class Location {

    private SharedPreferences sp;
    private Context context;

    @SuppressLint("CommitPrefEdits")
    public Location (Context context){
        this.context = context;
        sp = context.getSharedPreferences(context.getString
                (R.string.local_storage_appName), Context.MODE_PRIVATE);
    }

    public String getCurrentLocation(){
        int loc = sp.getInt(context.getString(R.string.local_storage_location), 0);
        String[] locationArr = context.getResources().getStringArray(R.array.locations);

        return locationArr[loc];
    }

    public void setCurrentLocation(String location){
        SharedPreferences.Editor ed = sp.edit();
        String[] locationArr = context.getResources().getStringArray(R.array.locations);
        for(int i = 0; i < locationArr.length; i++){
            Log.e("something", "5 times?");
            if(location.equals(locationArr[i])){
                Log.e("Set location: ", locationArr[i]);
                ed.putInt(context.getString(R.string.local_storage_location), i);
            }
        }
        ed.apply();
    }

    public int getLocationIndex(){
        return sp.getInt(context.getString(R.string.local_storage_location), 0);
    }
}
