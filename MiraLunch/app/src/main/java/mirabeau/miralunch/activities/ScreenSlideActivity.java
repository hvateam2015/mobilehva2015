package mirabeau.miralunch.activities;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import mirabeau.miralunch.R;
import mirabeau.miralunch.fragments.GuestFragment;
import mirabeau.miralunch.fragments.MainFragment;
import mirabeau.miralunch.fragments.MenuCardFragment;
import mirabeau.miralunch.fragments.SettingsFragment;
import mirabeau.miralunch.models.Location;
import mirabeau.miralunch.services.GeofenceTransitionsIntentService;
import mirabeau.miralunch.services.GoogleClient;
import mirabeau.miralunch.utils.PermissionDetector;
import mirabeau.miralunch.view_components.SlidingTabLayout;

public class ScreenSlideActivity extends FragmentActivity {
    public static final int NUM_PAGES = 3;      //The number of pages (wizard steps) to show in this demo.
    private ViewPager mPager;   //The pager widget, which handles animation and allows swiping horizontally to access previous and next wizard steps.
    private PermissionDetector mPD;
    private static final String[] INITIAL_PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;
    private static final String STATE_RESOLVING_ERROR = "resolving_error";
    private IntentFilter intentFilter = new IntentFilter();
    private GoogleClient googleClient;
    public Boolean accessLocation;

    public static GuestFragment guestFragment;
    public static MainFragment mainFragment;
    public static SettingsFragment settingsFragment;
    public static MenuCardFragment menuFragment;
    private Location loc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPD = new PermissionDetector(getApplicationContext());
        setContentView(R.layout.activity_screen_slide);
        // Instantiate a ViewPager and a PagerAdapter.
        PagerAdapter mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager());
        googleClient = new GoogleClient(this);

        accessLocation = hasAccessLocation();
        googleClient.connect();
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(2);
        mPager.setAdapter(mPagerAdapter);
        int startingFragmentPage = 1;
        mPager.setCurrentItem(startingFragmentPage);

        //Slider Tab Configuration
        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.tab_layout);
        mSlidingTabLayout.setViewPager(mPager);

        mResolvingError = savedInstanceState != null
                && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);
        intentFilter = new IntentFilter(GeofenceTransitionsIntentService.TAG);
        /*
            * Adds conditions to the BroadCast receiver
         */
        intentFilter.addAction(GeofenceTransitionsIntentService.ACTIONENTER);
        intentFilter.addAction(GeofenceTransitionsIntentService.CHANGEPREFERENCES);
        intentFilter.addAction(GeofenceTransitionsIntentService.ACTIONEXIT);

        guestFragment = new GuestFragment();
        mainFragment = new MainFragment();
        loc = new Location(getApplicationContext());
        menuFragment = new MenuCardFragment();
        menuFragment.setParameters(loc);
        settingsFragment = new SettingsFragment();
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    /**
     * A simple pager adapter that represents 3 MainsSlideFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private String[] tabNames = {
                getString(R.string.tab_settings),
                getString(R.string.tab_status),
                getString(R.string.tab_menu)
        };

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabNames[position];
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return settingsFragment;
                case 1:
                    return mainFragment;
                case 2:
                    return menuFragment;
                default:
                    return mainFragment;
            }
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
    /*
     * Geo-Fence methods
     */
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            // goAsync()
            if(intent.getAction().equals(GeofenceTransitionsIntentService.ACTIONENTER)) {
                //change button
                Log.d("Empty if statement", "wat nu ?!?! ");
            }
            if(intent.getAction().equals(GeofenceTransitionsIntentService.CHANGEPREFERENCES)){
                mainFragment.changePresentButtons(false);
                SharedPreferences sp = getApplicationContext().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putBoolean(getString(R.string.local_storage_present), true);
                editor.apply();
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        mPD.permissionsCheck(this);
        if (mPD.isNetworkAvailable()) {
            menuFragment.getMenu(loc.getCurrentLocation());
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if(!googleClient.isConnecting() && !googleClient.isConnected())
                    googleClient.connect();
            }
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_RESOLVING_ERROR, mResolvingError);
    }

   /*
     * @return boolean = true if the user has COARSE && FINE location rights
     */
    public boolean hasAccessLocation(){
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION) && hasPermission((Manifest.permission.ACCESS_COARSE_LOCATION)));
    }

    public boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this, perm));
    }

    public String[] getPermissionList(){
        return INITIAL_PERMISSIONS;
    }
}