package mirabeau.miralunch.fragments;

import android.Manifest;
import android.app.Fragment;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import mirabeau.miralunch.R;
import mirabeau.miralunch.activities.ScreenSlideActivity;
import mirabeau.miralunch.models.Location;
import mirabeau.miralunch.services.AsyncGET;
import mirabeau.miralunch.utils.PermissionDetector;
import mirabeau.miralunch.utils.ShakeDetector;
import mirabeau.miralunch.utils.Urls;


public class MenuCardFragment extends Fragment implements ShakeDetector.Listener {
    private Location location;
    public PermissionDetector mPD;
    ShakeDetector sd = new ShakeDetector(this);
    private int year;
    private int week;
    private TextView mondayTag;
    private TextView tuesdayTag;
    private TextView wednesdayTag;
    private TextView thursdayTag;
    private TextView fridayTag;
    private JSONArray jsonArray;
    public MenuCardFragment(){}
    View view;
    String TAG = "MenuCardFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPD = new PermissionDetector(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        sd.stop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_menu_card, container, false);
        SensorManager sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        sd.start(sensorManager);
        if (doesUserHaveFineLocationPermission() &&  doesUserHaveFineCoursePermission()) {
            initMenuSpinner();
        }
        return view;
    }
    @Override
    public void onResume() {super.onResume();}

    private boolean doesUserHaveFineLocationPermission()
    {
        int result = getActivity().checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }
    private boolean doesUserHaveFineCoursePermission()
    {
        int result = getActivity().checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    public void menuItemEmpty(int index){
        switch(index){
            case 0:
                mondayTag.setText(getString(R.string.weekmenu_day_empty));
                break;
            case 1:
                tuesdayTag.setText(getString(R.string.weekmenu_day_empty));
                break;
            case 2:
                wednesdayTag.setText(getString(R.string.weekmenu_day_empty));
                break;
            case 3:
                thursdayTag.setText(getString(R.string.weekmenu_day_empty));
                break;
            case 4:
                fridayTag.setText(getString(R.string.weekmenu_day_empty));
                break;
            default:
                break;
        }
    }

    public void setMenu(int index){
        try {
            switch (index) {
                case 0:
                    mondayTag.setText(jsonArray.getJSONObject(0).getString(getString(R.string.json_dish)));
                    break;
                case 1:
                    tuesdayTag.setText(jsonArray.getJSONObject(1).getString(getString(R.string.json_dish)));
                    break;
                case 2:
                    wednesdayTag.setText(jsonArray.getJSONObject(2).getString(getString(R.string.json_dish)));
                    break;
                case 3:
                    thursdayTag.setText(jsonArray.getJSONObject(3).getString(getString(R.string.json_dish)));
                    break;
                case 4:
                    fridayTag.setText(jsonArray.getJSONObject(4).getString(getString(R.string.json_dish)));
                    break;
                default:
                    break;
            }
        } catch(JSONException e){
            e.printStackTrace();
        }
    }


    public void getMenu(String location){
        new AsyncGET() {

            @Override
            protected void onPostExecute(JSONObject jsonObject) {

                mondayTag = (TextView) view.findViewById(R.id.menu_card_textview_monday_meal);
                tuesdayTag = (TextView) view.findViewById(R.id.menu_card_textview_tuesday_meal);
                wednesdayTag = (TextView) view.findViewById(R.id.menu_card_textview_wednesday_meal);
                thursdayTag = (TextView) view.findViewById(R.id.menu_card_textview_thursday_meal);
                fridayTag = (TextView) view.findViewById(R.id.menu_card_textview_friday_meal);

                jsonArray = new JSONArray();
                try {
                    if (jsonObject == null) {
                        //set layout to show that the week hasn't been created yet
                        mondayTag.setText(getString(R.string.weekmenu_day_empty));
                        tuesdayTag.setText(getString(R.string.weekmenu_day_empty));
                        wednesdayTag.setText(getString(R.string.weekmenu_day_empty));
                        thursdayTag.setText(getString(R.string.weekmenu_day_empty));
                        fridayTag.setText(getString(R.string.weekmenu_day_empty));
                        Log.e(TAG, "onPostExecute jsonObject is empty");
                    } else {
                        jsonArray = jsonObject.getJSONArray(getString(R.string.json_all_days));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                    //change the dishes on the layout
                    try {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            if (jsonArray.getJSONObject(i).getString(getString(R.string.json_dish)).equals(getString(R.string.weekmenu_day_empty_json))) {
                                if (i == (Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 2)) {
                                    ScreenSlideActivity.mainFragment.todayMainScreenTag.setText(getString(R.string.weekmenu_day_empty));
                                }
                                menuItemEmpty(i);
                            } else {
                                if (i == (Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 2)) {
                                    ScreenSlideActivity.mainFragment.todayMainScreenTag.setText(jsonArray.getJSONObject(i).getString(getString(R.string.json_dish)));
                                }
                                setMenu(i);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    super.onPostExecute(jsonObject);
            }
        }.execute(Urls.getCurrentWeek(location, getWeek(), getYear()));
    }

    public void setSpinnerSelection(){
        Spinner spinner = (Spinner) view.findViewById(R.id.menu_card_spinner_locations);
        Location location = new Location(getActivity().getApplicationContext());
        spinner.setSelection(location.getLocationIndex());
    }

    public void initMenuSpinner() {
            final Spinner spinner = (Spinner) view.findViewById(R.id.menu_card_spinner_locations);
            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity().getBaseContext(),
                    R.array.locations, android.R.layout.simple_spinner_item);
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            spinner.setAdapter(adapter);

            spinner.setSelection(location.getLocationIndex());

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                    String selected = spinner.getSelectedItem().toString();
                    getMenu(selected);
                    Log.i("Selected item : ", selected);
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {

                }

            });
    }

    public void setParameters(Location location){
        this.week = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
        this.year = Calendar.getInstance().get(Calendar.YEAR);
        this.location = location;
    }
    public Location getLocation() {
        return location;
    }

    public int getYear() {
        return year;
    }

    public int getWeek() {
        return week;
    }

    public void hearShake() {
        if (!mondayTag.getText().toString().contains(getString(R.string.weekmenu_day_spek))) {
            mondayTag.setText(mondayTag.getText() + getString(R.string.weekmenu_day_spek));
            tuesdayTag.setText(tuesdayTag.getText() + getString(R.string.weekmenu_day_spek));
            wednesdayTag.setText(wednesdayTag.getText() + getString(R.string.weekmenu_day_spek));
            thursdayTag.setText(thursdayTag.getText() + getString(R.string.weekmenu_day_spek));
            fridayTag.setText(fridayTag.getText() + getString(R.string.weekmenu_day_spek));
        }
    }
}