package mirabeau.miralunch.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Date;

import mirabeau.miralunch.R;
import mirabeau.miralunch.activities.ScreenSlideActivity;
import mirabeau.miralunch.models.Headcount;
import mirabeau.miralunch.utils.CountDown;
import mirabeau.miralunch.utils.Utils;

public class MainFragment extends Fragment{


    private Headcount headcount;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public TextView todayMainScreenTag;

    public static View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_main, container, false);

        headcount = new Headcount();

        CountDown.createCountDown(view, getActivity().getApplicationContext());

        todayMainScreenTag = (TextView) view.findViewById(R.id.lunch_menu_home_today);

        preferences = this.getActivity().getSharedPreferences(getString(R.string.local_storage_appName), Context.MODE_PRIVATE);
        editor = preferences.edit();
        int selectedLocation;
        selectedLocation = preferences.getInt(getString(R.string.local_storage_location), 0);
        String[] tango = getResources().getStringArray(R.array.locations);
        SpannableString ss = new SpannableString(tango[selectedLocation]);
        ss.setSpan(new UnderlineSpan(), 0, tango[selectedLocation].length(), 0);
        final TextView tv = (TextView) view.findViewById(R.id.main_lunch_status_button_location);
        tv.setText(ss);

        tv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int selectedLocation = preferences.getInt(getString(R.string.local_storage_location), 0);
                new AlertDialog.Builder(getActivity())
                        .setSingleChoiceItems(R.array.locations, selectedLocation, null)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                                int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                                // Do something useful withe the position of the selected radio button
                                String[] tango = getResources().getStringArray(R.array.locations);
                                SpannableString ss = new SpannableString(tango[selectedPosition]);
                                ss.setSpan(new UnderlineSpan(), 0, tango[selectedPosition].length(), 0);
                                tv.setText(ss);
                                editor.putInt(getString(R.string.local_storage_location), selectedPosition);
                                editor.apply();

                                ScreenSlideActivity.menuFragment.getMenu(tango[selectedPosition]);
                                ScreenSlideActivity.menuFragment.setSpinnerSelection();

                                headcount.changeLocationInService(view.getContext());

                            }
                        })
                        .show();
            }
        });
        createButtons();

        return view;
    }


    //All the buttons that are in the application
    public void createButtons(){
        Button presentBtn = (Button) view.findViewById(R.id.main_btn_present);
        Button notPresentBtn = (Button) view.findViewById(R.id.main_btn_not_present);
        Button addGuest = (Button) view.findViewById(R.id.main_btn_addGuest);
        presentBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!checkIfParticipating())
                    changePresentButtons(checkIfParticipating());
            }
        });
        notPresentBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (checkIfParticipating())
                    changePresentButtons(checkIfParticipating());
            }
        });

        addGuest.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GuestFragment guestFragment = ScreenSlideActivity.guestFragment;
                guestFragment.show(getActivity().getFragmentManager(), "GuestFragment");
            }
        });

        initButtons();
    }

    public void initButtons(){
        RelativeLayout relLayout =  (RelativeLayout) view.findViewById(R.id.main_lunch_status_block);
        Button present = (Button) view.findViewById(R.id.main_btn_present);
        Button notPresent = (Button) view.findViewById(R.id.main_btn_not_present);
        Button addGuest = (Button) view.findViewById(R.id.main_btn_addGuest);
        boolean presentBool = checkIfParticipating();

        if (presentBool) {
            present.setSelected(true);                  //select "present"
            present.setTextColor(Utils.getColorFromRes(view.getContext(), R.color.white));
            notPresent.setSelected(false);              //deselect "not present"
            notPresent.setTextColor(Utils.getColorFromRes(view.getContext(), R.color.black));
            relLayout.setVisibility(RelativeLayout.VISIBLE);   //show the relativeLayout

            //enable add guest button
            addGuest.setClickable(true);
            addGuest.setVisibility(View.VISIBLE);
            
        } else {
            present.setSelected(false);                 //deselect "present"
            present.setTextColor(Utils.getColorFromRes(view.getContext(), R.color.black));
            notPresent.setSelected(true);               //select "not present"
            notPresent.setTextColor(Utils.getColorFromRes(view.getContext(), R.color.white));
            relLayout.setVisibility(RelativeLayout.INVISIBLE); //hide the relativeLayout

            //disable add guest button
            addGuest.setClickable(false);
            addGuest.setVisibility(View.INVISIBLE);
        }
    }



    //checks if user is signed in for today's lunch.
    public boolean checkIfParticipating(){
        //check Database function
        return preferences.getBoolean(getString(R.string.local_storage_present), false);
    }

    public void changePresentButtons(boolean presentBool){
        //change layout of buttons
        Button addGuest = (Button) view.findViewById(R.id.main_btn_addGuest);
        RelativeLayout relLayout =  (RelativeLayout) view.findViewById(R.id.main_lunch_status_block);
        Button present = (Button) view.findViewById(R.id.main_btn_present);
        Button notPresent = (Button) view.findViewById(R.id.main_btn_not_present);

        if (!presentBool) {
            present.setSelected(true);                  //select "present"
            present.setTextColor(Utils.getColorFromRes(view.getContext(), R.color.white));
            notPresent.setSelected(false);              //deselect "not present"
            notPresent.setTextColor(Utils.getColorFromRes(view.getContext(), R.color.black));
            relLayout.setVisibility(RelativeLayout.VISIBLE);   //show the relativeLayout

            //enable add guest button
            addGuest.setClickable(true);
            addGuest.setVisibility(View.VISIBLE);

            // local storage change value.
            editor.putBoolean(getString(R.string.local_storage_present), true);
            editor.commit();
            // update headcount in mongodb
            headcount.updateHeadcountInService(view.getContext(), view.getContext().getString(R.string.toast_addCount));

        } else {
            present.setSelected(false);                 //deselect "present"
            present.setTextColor(Utils.getColorFromRes(view.getContext(), R.color.black));
            notPresent.setSelected(true);               //select "not present"
            notPresent.setTextColor(Utils.getColorFromRes(view.getContext(), R.color.white));
            relLayout.setVisibility(RelativeLayout.INVISIBLE); //hide the relativeLayout

            //disable add guest button
            addGuest.setClickable(false);
            addGuest.setVisibility(View.INVISIBLE);

            // local storage change value.
            editor.putBoolean(getString(R.string.local_storage_present), false);
            editor.putInt(getString(R.string.local_storage_guest), 0);
            editor.commit();

            //delete headcount in mongodb
            headcount.deleteHeadcountInService(view.getContext());

        }
        //set initial check date to today
        editor.putLong(getString(R.string.local_storage_initial_check), new Date().getTime());
    }
}

