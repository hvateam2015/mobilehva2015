package mirabeau.miralunch.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import mirabeau.miralunch.R;
import mirabeau.miralunch.models.Headcount;


public class GuestFragment extends android.app.DialogFragment {

    Context mContext;
    private Headcount headcount;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mContext = getActivity();
        headcount = new Headcount();
        if(mContext == null){
            Log.e("CONTEXT", "context is null");}
        else {

            //start dialog
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
            alertDialogBuilder.setTitle(getString(R.string.guest_title));

            //layout inside fragment dialog
                    LayoutInflater inflater = getActivity().getLayoutInflater();
            @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.fragment_guest, null);
            final NumberPicker numberPicker = (NumberPicker) view.findViewById(R.id.numberPicker);
            numberPicker.setMinValue(0);
            numberPicker.setMaxValue(30);
            numberPicker.setValue(headcount.getNumberOfGuestsFromPref(getActivity()));

            //disables soft keyboard
            numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
            alertDialogBuilder.setView(view);

            alertDialogBuilder.setNegativeButton(android.R.string.cancel, null);
            alertDialogBuilder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    headcount.setNumberOfGuestsFromPref(mContext, numberPicker.getValue());
                    headcount.updateHeadcountInService(mContext, mContext.getString(R.string.toast_addGuest));

                }
            });

            return alertDialogBuilder.create();
        }
        return null;
    }
}