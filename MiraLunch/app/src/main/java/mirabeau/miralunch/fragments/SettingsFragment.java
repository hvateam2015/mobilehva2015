package mirabeau.miralunch.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import mirabeau.miralunch.R;


public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getPreferenceManager().setSharedPreferencesName(getString(R.string.local_storage_appName));
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.fragment_settings);
    }
}