package mirabeau.miralunch.requests;

import android.content.Context;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import mirabeau.miralunch.models.Headcount;
import mirabeau.miralunch.models.Location;
import mirabeau.miralunch.utils.Utils;


public class HeadcountRequest {
    public HeadcountRequest(){ }


    public static Request createRequest(Context context, String url) {
        //generating ok http request
        return createRequest(url, getRequestBody_UpdateHeadcount(context));
    }


    private static RequestBody getRequestBody_UpdateHeadcount(Context context){
        Headcount headCount = new Headcount();
        int totalHeadCount = headCount.getHeadcount(context);
        Location location = new Location(context);
        return new FormEncodingBuilder()
                .add("count", Integer.toString(totalHeadCount))
                .add("location", location.getCurrentLocation())
                .add("device_id", Utils.getDeviceId(context))
                .build();

    }

    private static Request createRequest(String url,RequestBody formBody) {
        //generating ok http request
        return new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
    }
}
