package mirabeau.miralunch.fonts;


@SuppressWarnings("SpellCheckingInspection")
public final class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/BookAntiqua.ttf");
        //BookAntiqua is the standard text font. For more information, check the mirabeau corporate quidelines.
    }
}