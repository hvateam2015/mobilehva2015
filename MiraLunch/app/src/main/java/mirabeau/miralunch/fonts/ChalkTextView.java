package mirabeau.miralunch.fonts;


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class ChalkTextView extends TextView {

    public ChalkTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ChalkTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChalkTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/chalk.ttf");
        setTypeface(tf);
    }

}