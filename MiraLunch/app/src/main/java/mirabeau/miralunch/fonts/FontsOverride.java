package mirabeau.miralunch.fonts;


//This class overrides the standard font.
//The usage of this class is in the Application Class


import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.Log;

import java.lang.reflect.Field;

public final class FontsOverride {
    public static String TAG = "setDefaultFont";
    public static void setDefaultFont(Context context,
                                      String staticTypefaceFieldName, String fontAssetName) {
        final Typeface regular = Typeface.createFromAsset(context.getAssets(),
                fontAssetName);
        replaceFont(staticTypefaceFieldName, regular);
        Log.i(TAG, "Default font is being changed");

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    protected static void replaceFont(String staticTypefaceFieldName,
                                      final Typeface newTypeface) {
        try {
            Log.i(TAG, "type font changing succeeded");
            final Field staticField = Typeface.class
                    .getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            Log.e(TAG, "type font changing failed");
            e.printStackTrace();
        }
    }
}