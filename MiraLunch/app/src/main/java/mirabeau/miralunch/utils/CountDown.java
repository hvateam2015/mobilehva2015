package mirabeau.miralunch.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;

import java.util.Calendar;

import mirabeau.miralunch.R;


public class CountDown {

    //Declare textviews
    static TextView mHoursLabel;
    static TextView mMinutesLabel;
    static TextView mSecondsLabel;

    static View view;

    // Values displayed by the timer
    static int mDisplayDays;
    static int mDisplayHours;
    static int mDisplayMinutes;
    static int mDisplaySeconds;
    static final int secsInDay = 86400;
    static int hour = 12;
    static int minute = 0;
    static int second = 0;

    public static void createCountDown(View mainView, Context context){
        view = mainView;
        configureViews(context); //Initialize timers textviews

        // Timer setup: setting the new deadline
        // Get date from database
        Calendar currentDate = Calendar.getInstance();

        int month = currentDate.get(Calendar.MONTH);
        int year = currentDate.get(Calendar.YEAR);
        int tomorrow;
        //WEEKEND CHECK && BEFORE 12
        if(currentDate.get(Calendar.HOUR_OF_DAY) < 12 && currentDate.get(Calendar.DAY_OF_WEEK) != 7 && currentDate.get(Calendar.DAY_OF_WEEK) != 1){
            tomorrow = currentDate.get(Calendar.DAY_OF_MONTH);
        } else if(currentDate.get(Calendar.DAY_OF_WEEK) == 6 && currentDate.get(Calendar.HOUR_OF_DAY) >= 12){
            tomorrow = currentDate.get(Calendar.DAY_OF_MONTH) + 3;
        } else if (currentDate.get(Calendar.DAY_OF_WEEK) == 7) {
            tomorrow = currentDate.get(Calendar.DAY_OF_MONTH) + 2;
        } else {
            tomorrow = currentDate.get(Calendar.DAY_OF_MONTH) + 1;
        }
        Calendar nextLunch = Calendar.getInstance();
        nextLunch.set(year, month, tomorrow, hour, minute, second);
        long milliDiff = nextLunch.getTimeInMillis() - currentDate.getTimeInMillis();

        makeNewTimer(milliDiff);
    }

    public static void makeNewTimer(final long milliDiff){
        new CountDownTimer(milliDiff, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

                long secondsUntilFinished=millisUntilFinished/1000;
                // decompose difference into days, hours, minutes and seconds
                mDisplayDays = (int) (secondsUntilFinished / secsInDay);
                mDisplayHours = (int) ((secondsUntilFinished - (mDisplayDays * secsInDay)) / 3600) + (mDisplayDays * 24);
                mDisplayMinutes = (int) ((secondsUntilFinished % 3600) / 60);
                mDisplaySeconds = (int) (secondsUntilFinished % 60);

                mSecondsLabel.setText(formatCountDown(mDisplaySeconds + ""));
                mMinutesLabel.setText(formatCountDown(mDisplayMinutes + ""));
                mHoursLabel.setText(formatCountDown(mDisplayHours + ""));
                //set new count down
                if(milliDiff < 2000){
                    onFinish();
                }
            }

            @Override
            public void onFinish() {

                Calendar nextLunch = Calendar.getInstance();
                Calendar currentDate = Calendar.getInstance();

                int month = currentDate.get(Calendar.MONTH);
                int year = currentDate.get(Calendar.YEAR);
                int tomorrow;
                if(currentDate.get(Calendar.HOUR_OF_DAY) < 12 && currentDate.get(Calendar.DAY_OF_WEEK) != 7 && currentDate.get(Calendar.DAY_OF_WEEK) != 1){
                    tomorrow = currentDate.get(Calendar.DAY_OF_MONTH);
                } else if(currentDate.get(Calendar.DAY_OF_WEEK) == 6 && currentDate.get(Calendar.HOUR_OF_DAY) >= 12){
                    tomorrow = currentDate.get(Calendar.DAY_OF_MONTH) + 3;
                } else if (currentDate.get(Calendar.DAY_OF_WEEK) == 7) {
                    tomorrow = currentDate.get(Calendar.DAY_OF_MONTH) + 2;
                } else {
                    tomorrow = currentDate.get(Calendar.DAY_OF_MONTH) + 1;
                }

                nextLunch.set(year, month,tomorrow , hour, minute, second);
                long milliDiff = nextLunch.getTimeInMillis() - currentDate.getTimeInMillis();
                long secondsUntilFinished=milliDiff/1000;
                mDisplayDays = (int) (secondsUntilFinished / secsInDay);
                mDisplayHours = (int) ((secondsUntilFinished - (mDisplayDays * secsInDay)) / 3600);
                mDisplayMinutes = (int) ((secondsUntilFinished - ((mDisplayDays * secsInDay) + (mDisplayHours * 3600))) / 60);
                mDisplaySeconds = (int) (secondsUntilFinished % 60);

                mSecondsLabel.setText(formatCountDown(mDisplaySeconds+""));
                mMinutesLabel.setText(formatCountDown(mDisplayMinutes+""));
                mHoursLabel.setText(formatCountDown(mDisplayHours + ""));
                makeNewTimer(milliDiff);
            }
        }.start();
    }

    //initiate the countdown textviews
    public static void configureViews(Context context) {
        mHoursLabel = (TextView) view.findViewById(R.id.mHours);
        mMinutesLabel = (TextView) view.findViewById(R.id.mMinutes);
        mSecondsLabel = (TextView) view.findViewById(R.id.mSeconds);
        Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/digital.ttf");
        mHoursLabel.setTypeface(type);
        mMinutesLabel.setTypeface(type);
        mSecondsLabel.setTypeface(type);
    }
    public static String formatCountDown(String countDownTextview){
        if(countDownTextview.length()==1){
            return "0"+countDownTextview;
        }else {
            return countDownTextview;
        }
    }
}