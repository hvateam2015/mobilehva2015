package mirabeau.miralunch.utils;

import android.content.Context;
import android.os.Build;
import android.provider.Settings.Secure;
import android.support.v4.content.ContextCompat;



public class Utils {

    @SuppressWarnings("deprecation")
    public static int getColorFromRes(Context context,int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    public static String getDeviceId(Context context){

        return Secure.getString(context.getContentResolver(),
               Secure.ANDROID_ID);
    }

}
