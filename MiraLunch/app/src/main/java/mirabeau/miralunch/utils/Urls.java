package mirabeau.miralunch.utils;


public class Urls {

    public Urls(){}
    private static final String BASE = "http://miraservice-linkedmd.rhcloud.com/";
    private static final String CURRENT_WEEK = BASE + "menu/get/";
    private static final String GET_GEOFENCES = "http://miraservice-linkedmd.rhcloud.com/geofence/getAll/";
    private static final String SET_HEADCOUNT = BASE + "headcount/edit/update";
    private static final String UPDATE_HEADCOUNT_LOCATION = BASE + "headcount/edit/changeLocation";
    private static final String DELETE_HEADCOUNT = BASE + "headcount/edit/delete";

    public static String getCurrentWeek(String location, int week, int year){
        return CURRENT_WEEK + location + "/" + week + "/" + year;
    }

    public static String getSetHeadcount() {
        //Usage: post request with variable "headcountUpdate" and the headcount value you want to set/
        return SET_HEADCOUNT;
    }

    public static String getLocationUpdate(){
        return UPDATE_HEADCOUNT_LOCATION;
    }

    public static String getGeofences() {
        return GET_GEOFENCES;
    }

    public static String getDeleteHeadcount() {
        //Usage: post request with variable "headcountUpdate" and the headcount value you want to set/
        return DELETE_HEADCOUNT;
    }
}