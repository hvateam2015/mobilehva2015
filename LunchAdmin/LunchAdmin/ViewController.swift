//
//  ViewController.swift
//  LunchAdmin
//
//  Created by Macbook on 16-09-15.
//  Copyright (c) 2015 Mirabeau. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var webView: UIWebView!
    let url = "http://miradmin-linkedmd.rhcloud.com/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let requestURL = NSURL(string:url){
            let request = NSURLRequest(URL: requestURL)
            webView.loadRequest(request);
            webView.scrollView.bounces = false;
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

